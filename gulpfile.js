const gulp = require('gulp')
const nodemon = require('gulp-nodemon')
const {spawn} = require('child_process')

gulp.task('dev', function () {
  nodemon({script: __dirname + '/server/index.js'})
  spawn(__dirname + '/node_modules/.bin/webpack', {
    cwd: __dirname + '/client',
    stdio: 'inherit'
  })
})
