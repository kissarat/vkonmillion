import React, {Component} from 'react'
import Menu from '../widget/menu.jsx'
import importLink from '../widget/link.jsx'

const styleLink = importLink('/hydrogen.css')

const menu = [
  {
    name: 'Продукты',
    url: '/products',
  }
]

export default class App extends Component {
  render() {
    return <div id="hydrogen" className="layout">
      {styleLink}
      <header>
        <div className="title">Заголовок сайта</div>
        <nav>
          <Menu items={menu}/>
        </nav>
      </header>
      <div id="main">
        <main>{this.props.children}</main>
      </div>
    </div>
  }
}
