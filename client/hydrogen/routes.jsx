import React from 'react'
import {Route} from 'react-router'
import App from './app.jsx'
import ProductList from './order/product.jsx'
import Order from './order/index.jsx'

const routes = <Route component={App}>
  <Route path="order" component={Order}/>
  <Route path="products" component={ProductList}/>
</Route>

export default routes
