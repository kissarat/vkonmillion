import React, {Component} from 'react'
import {Button} from 'react-bootstrap'

const products = [
  {
    id: 1,
    name: 'Сахар',
    price: 400,
    image: 'http://kremen.today/wp-content/uploads/2015/10/x233626_default.jpg.pagespeed.ic.Wy5m4hGiTL.jpg'
  },
  {
    id: 2,
    name: 'Полынь',
    price: 250,
    image: 'http://lechebnaya-lavka.com.ua/images/polezno/polun.jpg'
  },
  {
    id: 3,
    name: 'Абсент',
    price: 150,
    image: 'http://typobar.ru/files/drinks/vinsent_van_gog_premium_1.png'
  }
]

const Product = ({id, image, name, price, select}) => <div className="product">
  <div className="info">
    <h2>{name}</h2>
    <div className="image" style={{backgroundImage: `url("${image}")`}}/>
    <div className="price">
      <span className="name">Цена</span>
      <span className="value money">{price}</span>
    </div>
    <Button bsStyle="primary" onClick={() => select(id)}>Купить</Button>
  </div>
</div>

export default class ProductList extends Component {
  render() {
    const items = products.map(p => <Product key={p.name} {...p} select={this.props.select} />)
    return <div className="page product-list">
      <h1>Товары</h1>
      <div className="list">{items}</div>
    </div>
  }
}
