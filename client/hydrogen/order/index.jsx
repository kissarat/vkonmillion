import React, {Component} from 'react'
import ProductList from './product.jsx'
import {Button} from 'react-bootstrap'
import {Select, fieldList} from '../../form/field.jsx'
import {map, isObject} from 'lodash'

const deliveryMethods = {
  place: 'Доставка на район',
  mail: 'Доставка почтой'
}

const mails = {
  1: 'Нова пошта',
  2: 'Укрпошта',
  3: 'Ін-Тайм'
}

const paymentSystem = {
  bitcoin: 'Bitcoin',
  payeer: 'Payeer'
}
