import React, {Component} from 'react'
import {loadUser} from './utils.jsx'
import {Link} from 'react-router'
import api from '../connect/api.jsx'
import {Social, Contacts, Name, About, Avatar} from '../widget/card.jsx'

export default class Main extends Component {
  componentWillMount() {
    loadUser.call(this)
    api.get('user/last').then(signups => this.setState({signups}))
  }

  render() {
    const signups = (this.state.signups || []).map(({id, avatar, forename, surname}) =>
      <li className="new-signup" key={id}>
        <a className="vk" href={'https://vk.com/id' + id} target="_blank">
          <div className="avatar" style={{backgroundImage: `url("${avatar}")`}}/>
          <div className="name">
            <span className="first">{forename}</span>
            &nbsp;
            <span className="last">{surname}</span>
          </div>
        </a>
      </li>
    );
    const ref = this.state.ref
    const sponsor = ref
      ? <div className="cabinet-sponsor">
      <Avatar {...ref}/>
      <div className="cabinet-sponsor-name">
        <h2>Ваш спонсор</h2>
        <i>Свяжитесь, чтобы получить рекомендации по заработку</i>
        <Name {...ref}/>
      </div>
      <div className="cabinet-sponsor-contact">
        <Social {...ref}/>
        <Contacts {...ref}/>
        <About {...ref}/>
      </div>
    </div>
      : ''
    return <div className="cabinet-page cabinet-main">
      <h1>Личный кабинет</h1>
      <div className="cabinet-main-middle">
        <div className="cabinet-informers">
          <div className="informer chess">
            <div className="amount money">{this.state.income}</div>
            <i>Заработано с шахматного маркетинга</i>
            <div className="today money">{this.state.day || '0'}</div>
          </div>
          <div className="informer referral">
            <div className="amount money">0</div>
            <i>Заработано с реферальных начислений</i>
            <div className="today monry">0</div>
          </div>
          <div className="informer enters">
            <div className="amount">{this.state.goref}</div>
            <i>Переходов по реферальной ссылке</i>
            <div className="today">{this.state.goref_day || '0'}</div>
          </div>
          <div className="informer new-friends">
            <div className="amount">{this.state.friends || '0'}</div>
            <i>Новых друзей Вконтакте</i>
            <div className="today">{this.state.friends_day || '0'}</div>
          </div>
        </div>
        <div className="new-signups">
          <h2>Новые регистрации</h2>
          <ul>{signups}</ul>
        </div>
      </div>
      <div className="cabinet-main-bottom">
        <div className="cabinet-dossier">
          <h2>Досье на Вас</h2>
          <ul>
            <li className="name">
              <div className="label">ФИО</div>
              <Name {...this.state}/>
            </li>
            <li className="skype">
              <div className="label">Skype</div>
              <div className="value">{this.state.skype}</div>
            </li>
            <li className="wallet">
              <div className="label">Кошелек</div>
              <div className="value">{this.state.payeer}</div>
            </li>
            <li className="created">
              <div className="label">В проекте</div>
              <div className="value">{new Date(this.state.created).toLocaleDateString()}</div>
            </li>
          </ul>
          <Link to="/settings">Изменить</Link>
        </div>
        {sponsor}
      </div>
    </div>
  }
}
