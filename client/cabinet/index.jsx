import React, {Component} from 'react'
import Main from './main.jsx'
import Settings from './settings.jsx'
import Structure from './structure.jsx'
import InviteList from './invite.jsx'
import {loadUser} from './utils.jsx'
import api from '../connect/api.jsx'
import {each} from 'lodash'
import Menu from '../widget/menu.jsx'
import importLink from '../widget/link.jsx'
import {Link, browserHistory} from 'react-router'
import i18n from '../ui/i18n'
import {Header} from '../ui/app.jsx'
import {isClient} from '../globals'

export {Main, Settings, Structure, InviteList}

const styleLink = importLink('/style.css')

let menu

export class Cabinet extends Component {
  componentWillReceiveProps(props) {
    if (props.params.type && props.params.data) {
      let type = props.params.type
      if ('error' === type) {
        type = 'danger'
      }
      const data = JSON.parse(atob(props.params.data))
      let message = i18n(data[0], data[1])
      if (data[2]) {
        message = <div key="main">
          <div className="message">{message}</div>
          <div className="json">{JSON.stringify(data[2])}</div>
        </div>
      }
      this.setState({alert_external: {type, message}})
    }
    else {
      this.setState({alert_external: false})
    }
  }

  floatMenu = (e) => {
    if (this.refs.navigation) {
      if (this.navBox) {
        if ('resize' === e.type) {
          const box = this.refs.navigation.getBoundingClientRect()
          this.navBox.left = box.left
        }
      }
      else {
        const box = this.refs.navigation.getBoundingClientRect()
        this.navBox = {
          top: box.top,
          left: box.left
        }
      }
      const isFloating = scrollY > this.navBox.top
      this.refs.navigation.classList.toggle('float-vertical', isFloating)
      if (isFloating) {
        this.refs.navigation.style.left = this.navBox.left + 'px'
      }
      else {
        this.refs.navigation.style.removeProperty('left')
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.floatMenu)
    window.removeEventListener('resize', this.floatMenu)
  }

  componentWillMount() {
    if (isClient && 'classList' in Element.prototype) {
      window.addEventListener('resize', this.floatMenu)
      window.addEventListener('scroll', this.floatMenu)
    }

    loadUser.call(this, false, false).then((user) => {
      menu = [
        {name: 'Кабинет', url: '/cabinet', icon: 'fa fa-user-circle-o'},
        {name: 'Маркетинг', url: '/marketing.html', icon: 'fa fa-briefcase', external: '_blank'},
        {name: 'Настройки', url: '/settings', icon: 'fa fa-cogs'},
        {name: 'Структура', url: '/structure/referral', icon: 'fa fa-sort-amount-asc'},
        {name: 'Спонсоры', url: '/structure/sponsor', icon: 'fa fa-address-card-o'},
        {name: 'Получить Друзей', url: '/invite', icon: 'fa fa-users'},
        {name: 'Финансы', url: '/transfer', icon: 'fa fa-rub'},
        {name: 'Скайп-чат поддержки', url: api.config.feedback, icon: 'fa fa-skype', external: true},
        {name: 'Выход', url: '/logout', icon: 'fa fa-sign-out'}
      ]
      if (user.admin) {
        menu.push({name: 'Администрирование', url: '/admin', icon: 'fa fa-user-secret'})
      }
      user.menu = menu
      this.setState(user)
    })
    this.componentWillReceiveProps(this.props)
  }

  clipboardURL(url) {
    if (window.clipboardData && clipboardData.setData instanceof Function) {
      clipboardData.setData('URL', url)
    }
    else if (document.execCommand instanceof Function) {
      const input = document.getElementById('copy-referral')
      input.select()
      document.execCommand('copy')
    }
  }

  withdraw = () => {
    this.setState({
      alert_auto: {
        type: 'info',
        message: `Вывод осуществляется автоматически, сразу после оплаты ваших рефералов.
          Если выплата не осуществилась сразу, то она будет осуществлена после подтверждения
          администрации в течении сутки`
      }
    })
    browserHistory.push('/transfer')
  }

  render() {
    const payURL = `/vip`
    const {id, avatar, forename, surname, balance, income, day} = this.state
    const activeUntil = new Date(this.state.active)
    const active = activeUntil.getTime() > Date.now()
      ? <div className="user-active">
      <span className="status">{activeUntil.toLocaleDateString()}</span>
    </div>
      : <div className="user-no-active">
      <span className="status">Не активен</span>
      <a href={payURL} className="btn btn-primary">Продлить</a>
    </div>
    let alert = []
    each(this.state, function (value, key) {
      if (key.indexOf('alert_') === 0 && value.type) {
        alert.push(<div key={key} className={'alert alert-' + value.type}>{value.message}</div>)
      }
    })

    const menu = this.state.menu ? <div className="navigation" ref="navigation">
      <h2>Навигация</h2>
      <Menu items={this.state.menu}/>
    </div>
      : ''

    const refURL = id ? location.origin + '/ref/' + (376396917 == id ? 392556874 : id) : ''

    return <div className="cabinet layout">
      <Header/>
      <div className="img evgeniy banner">
        <a href="http://pomojem.lptrend.biz/" target="_blank">
          <img src="/images/evgeniy.jpg" alt="ЗАПИШИСЬ НА ОБУЧЕНИЕ"/>
        </a>
        <a href="http://pomojem.lptrend.biz/" className="evgeniy" target="_blank">
          Запишись на обучение
        </a>
      </div>
      <div className="container">
        <div className="cabinet-left">
          <div className="about">
            <div className="cabinet avatar" style={{backgroundImage: `url("${avatar}")`}}></div>
            <div className="name">
              <span className="first">{forename}</span>
              &nbsp;
              <span className="last">{surname}</span>
            </div>
            <div className="active">{active}</div>
          </div>
          {menu}
        </div>
        <div className="cabinet-right">
          <div className="top-widget">
            <div className="referral">
              <h2>Реферальная ссылка</h2>
              <i>Нажмите, чтобы скопировать</i>
              <input
                readOnly={true}
                id="copy-referral"
                onClick={() => this.clipboardURL(refURL)}
                value={refURL}/>
            </div>
            <div className="balance-widget">
              <div className="balance-left"/>
              <div className="balance-right">
                <div className="balance-content">
                  <div className="balance">{balance || '0'}</div>
                  <div className="balance-income">
                    <div className="income money">{income || '0'}</div>
                    <div className="today money">{day || '0'}</div>
                  </div>
                </div>
                <div className="balance-buttons">
                  <Link to={payURL} className="btn btn-primary">Пополнить</Link>
                  <div className="btn btn-success" onClick={this.withdraw}>Вывести</div>
                </div>
              </div>
            </div>
          </div>
          <div className="content">
            {alert}
            {this.props.children}
          </div>
        </div>
      </div>
      <footer>
        <div className="footer-container">
          <a className="payeer" href="https://payeer.com/?partner=32316">
            <img src="/images/payeer-logo.jpg"/>
          </a>
        </div>
      </footer>
    </div>
  }
}
