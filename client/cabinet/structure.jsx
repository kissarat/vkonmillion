import React, {Component} from 'react'
import api from '../connect/api.jsx'
import {each} from 'lodash'
import Busy from '../widget/busy.jsx'
import Card from '../widget/card.jsx'

export default class Structure extends Component {
  componentWillReceiveProps(props) {
    this.setState({busy: true})
    api.setTokenFromQuery()
    api.get('user/structure', {
      view: props.params.view,
      user_id: localStorage.getItem('user_id')
    })
      .then((users) => this.setState({
        busy: false,
        users
      }))
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    if (this.state.busy) {
      return <Busy>Загрузка списка людей...</Busy>
    }
    else {
      const users = this.state.users.map(u => <li key={u.id}><Card {...u}/></li>)
      const graphAnchor = 'sponsor' !== this.props.params.view
        ? <a className="tree" href={'/graph.html#' + localStorage.getItem('user_id')} target="_blank">Дерево партнеров</a>
        : ''
      return <div className="page invite">
        <h1>{'sponsor' === this.props.params.view ? 'Cпонсоры' : 'Структура'}</h1>
        {graphAnchor}
        <ul className="double">{users}</ul>
      </div>
    }
  }
}
