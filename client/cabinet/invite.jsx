import React, {Component} from 'react'
import Card from '../widget/card.jsx'
import Busy from '../widget/busy.jsx'
import api from '../connect/api.jsx'
import {map} from 'lodash'

export default class InviteList extends Component {
  componentWillReceiveProps(props) {
    this.setState({busy: true})
    api.setTokenFromQuery()
    api.get('vkontakte/invite', {user_id: localStorage.getItem('user_id')})
      .then(({candidates}) => this.setState({
        busy: false,
        candidates
      }))
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    if (this.state.busy) {
      return <Busy>Загрузка списка людей...</Busy>
    }
    else {
      const users = map(this.state.candidates, u => <li key={u.id}>
        <Card {...u}/>
        <a className="btn btn-primary" href={'https://vk.com/id' + u.id} target="_blank">Добавить в друзья</a>
      </li>)
      return <div className="page invite">
        <h1>Получить друзей</h1>
        <ul className="double">{users}</ul>
      </div>
    }
  }
}
