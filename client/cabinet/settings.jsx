import React, {Component} from 'react'
import {loadUser, setupFields} from './utils.jsx'
import {browserHistory} from 'react-router'
import api from '../connect/api.jsx'
import Form from '../form/form.jsx'
import {map, each} from 'lodash'

export default class Settings extends Component {
  componentWillReceiveProps(props) {
    api.setTokenFromQuery()
    loadUser.call(this, false, false).then((user) => {
      const state = {
        title: 'Настройка профиля',
        button: 'Сохранить',
        action: 'user/save',
        params: {
          id: user.id,
          time: Date.now()
        },
        fields: {
          payeer: {
            label: 'Payeer'
          },
          link: {
            label: 'Репост (ссылка)'
          },
          about: {
            label: 'Описание',
            rows: 6
          },
          email: {
            label: 'Почта'
          },
          skype: {
            label: 'Skype'
          },
          phone: {
            label: 'Телефон',
            input(value) {
              return value.replace(/^\d/, '')
            }
          },
          facebook: {
            label: 'Facebook'
          },
          odnoklassniki: {
            label: 'Odnoklassniki'
          },
          twitter: {
            label: 'Twitter'
          },
          instagram: {
            label: 'Instagram'
          }
        }
      }
      setupFields(user, state.fields, api.entities.user.fields)
      this.setState(state)
    })
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  onSuccessSubmit = (state) => {
    browserHistory.push('/cabinet')
  }

  render() {
    return <Form onSuccessSubmit={this.onSuccessSubmit} {...this.state}/>
  }
}
