import api from '../connect/api.jsx'
import Busy from '../widget/busy.jsx'
import React, {Component} from 'react'
import {browserHistory} from 'react-router'
import {each} from 'lodash'
import Card from '../widget/card.jsx'
import qs from '../base/urlencoded.jsx'
// import {isClient} from '../globals.jsx'

export default class Share extends Component {
  componentWillReceiveProps(props) {
    const setCard = current => this.setState({current, busy: false})
    this.state = {
      busy: true
    }
    if (props.params.id) {
      api.get('user/get', {id: props.params.id}).then(setCard)
    }
    api.get('user/shareCards').then(({cards, count}) => {
      if (count < 7) {
        if (props.params.id) {
          this.setState({cards, count})
        }
        else {
          browserHistory.push(`/share/1/${cards[0].id}`)
        }
      }
      else {
        browserHistory.push('/vip')
      }
    })
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  next = () => {
    const number = +this.props.params.number + 1
    browserHistory.push(this.state.count < 7
      ? `/share/${number}/${this.state.cards[number - 1].id}`
      : '/vip')
  }

  onClick = (e) => {
    e.preventDefault()
    const url = e.target.href

    const w = open(url, this.getName(),
      `left=100,top=100,width=${screen.width - 100},height=${screen.height - 100},toolbar=0,resizable=0`)
    const from = +e.target.id
    const timer = setInterval(() => {
      if (w.closed) {
        clearInterval(timer)
        this.setState({checkShareBusy: true})
        api.get('vkontakte/checkShare', {from}).then((r) => {
          this.setState({checkShareBusy: false})
          if (r.success) {
            this.next()
          }
          else if (401 === r.statusCode) {
            browserHistory.push('/enter')
          }
          else if (false === r.success) {
            const c = this.state.cards && this.props.params.number < this.state.cards.length
            if (c) {
              this.setState({
                error: c
                  ? 'Вы не опубликовали на своей стене'
                  : 'Нету больше свободных'
              })
            }
            else {
              browserHistory.push('/vip')
            }
          }
          else {
            this.setState({error: 'Произошла неизвестная ошибка: ' + JSON.stringify(r)})
          }
        })
      }
    }, 300)
  }

  onClickSkip = () => {
    if (this.state.cards instanceof Array) {
      this.next()
    }
    else {
      browserHistory.push('/share')
    }
  }

  getName() {
    return `${this.state.current.forename} ${this.state.current.surname}`
  }

  render() {
    const alert = this.state.error
      ? <div className="alert alert-danger">{this.state.error}</div>
      : ''
    let content
    if (this.state.busy || !this.state.current) {
      content = <Busy>Загрузка контакта пользователя...</Busy>
    }
    else {
      const vkShareParams = {
        title: this.getName(),
        image: this.state.current.avatar,
        url: this.state.current.link || ('http://vk-mm.com/ref/' + this.state.current.id)
      }
      if (this.state.current.about) {
        vkShareParams.description = this.state.current.about
      }

      const url = 'https://vk.com/share.php?' + qs.stringify(vkShareParams)
      const shareButton = this.state.checkShareBusy
        ? <Busy/>
        : [<a key="share"
              className="btn btn-primary"
              href={url}
              target="_blank"
              id={this.state.current.id}
              onClick={this.onClick}>
        Поделится
      </a>]
      if (shareButton instanceof Array && this.state.error) {
        shareButton.push(<span
          key="next"
          className="btn btn-warning"
          onClick={this.onClickSkip}>
          Пропустить
        </span>)
      }
      content = <div className="share-container">
        <Card {...this.state.current}/>
        <div className="share-buttons">{shareButton}</div>
      </div>
    }
    return <div className="page share step-3">
      <h1>Шаг №3</h1>
      <h2>Активируйте платформу</h2>
      <h3>Для активации вам нужно разместить запись на вашей стене</h3>
      <div className="count">
        Добавлено <span>{'number' === typeof this.state.count ? this.state.count : 'неизвестно'}</span>
        <span> из </span>
        <span>7</span>
      </div>
      {alert}
      {content}
    </div>
  }
}
