import {browserHistory} from 'react-router'
import api from '../connect/api.jsx'
import {each, defaults} from 'lodash'

export function loadUser(user, doSetState = true) {
  const _set = (user) => {
    if ('subscribed' === user.status) {
      user.busy = false
      if (!user.payeer) {
        user.alert_payeer = {
          type: 'warning',
          message: 'Укажите кошелек в настройках, иначе вам не будет начислена прибыль'
        }
      }
      if (doSetState) {
        this.setState(user)
      }
    }
    else if (!user.id) {
      browserHistory.push('/login')
    }
    else {
      browserHistory.push('/follow')
    }
    return user
  }

  this.setState({busy: true})
  if (api.setTokenFromQuery()) {
    browserHistory.push('/cabinet')
  }
  return api.get('user/me').then(_set)
}

export function setupFields(state, fields, schemaFields) {
  each(fields, (value, key) => {
    if (state[key]) {
      fields[key].value = state[key]
    }
    if (schemaFields && schemaFields[key]) {
      defaults(fields[key], schemaFields[key])
    }
  })
}
