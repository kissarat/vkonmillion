import React from 'react'
import {Router, Route, browserHistory} from 'react-router'

import api from './connect/api.jsx'
import {Cabinet, Main, Settings, Structure, InviteList} from './cabinet/index.jsx'
import {CandidateList, Follow} from './ui/friends.jsx'
import {Login, Signup, Logout, Enter} from './ui/auth.jsx'
import {Outdoor} from './ui/app.jsx'
import {Success, TransferList, Vip} from './ui/transfer.jsx'
import Share from './cabinet/share.jsx'
import Admin from './ui/admin.jsx'
import {NotFound, Unavailable, Development} from './ui/page.jsx'

export const routes = <Route path='/'>
  <Route component={Cabinet}>
    <Route path='cabinet' component={Main}/>
    <Route path='settings' component={Settings}/>
    <Route path='structure/:view' component={Structure}/>
    <Route path='alert/:type/:data' component={Main}/>
    <Route path='success' component={Success}/>
    <Route path='transfer' component={TransferList}/>
    <Route path='invite' component={InviteList}/>
    <Route path='admin' component={Admin}/>
  </Route>
  <Route path="logout" component={Logout}/>

  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='signup' component={Signup}/>
    <Route path='ref/:id' component={Signup}/>
    <Route path='candidates' component={CandidateList}/>
    <Route path='enter' component={Enter}/>
    <Route path='share' component={Share}/>
    <Route path='share/:number/:id' component={Share}/>
    <Route path='follow' component={Follow}/>
    <Route path='vip' component={Vip}/>
    <Route path='development' component={Development}/>
    <Route path='unavailable' component={Unavailable}/>
    <Route path='*' component={NotFound}/>
  </Route>
</Route>

export const router = <Router history={browserHistory}>{routes}</Router>

export default function main(render) {
  return api.get('/config')
    .then(function (config) {
      api.config = config
      if ('undefined' !== typeof VK) {
        VK.init({apiId: config.vkontakte.id})
      }
      try {
        render(router)
      }
      catch (ex) {
        console.error('Rendering error', ex)
      }
    })
    .catch(err => render(<Unavailable/>, err))
}

if ('undefined' !== typeof window && window.document) {
  const {render} = require('react-dom')
  main(function (root, err) {
    if (err) {
      console.error(err)
    }
    render(root, document.getElementById('app'))
  })
}
