import {some, each, filter} from 'lodash'
import {debug} from '../globals'

export function sortPrimitive(order) {
  return function (a, b) {
    if (a === b) {
      return 0
    }
    else if (!a) {
      return order
    }
    else if (!b) {
      return -order
    }
    else if ('string' === typeof a) {
      return a.localeCompare(b)
    }
    else {
      return a - b
    }
  }
}

export function buildParamsFromEntity(entity) {
  const params = {}
  if (some(entity.fields, f => f.order)) {
    params.order = []
    each(entity.fields, function ({name, order}) {
      if (order) {
        params.order.push(order > 0 ? name : '-' + name)
      }
    })
    params.order = params.order.join('.')
  }
  params.select = filter(entity.fields, f => f.compact).map(f => f.name).join('.')
  debug(params)
  return params
}
