import api from '../connect/api.jsx'
import React, {Component} from 'react'
import Subscriber from '../connect/subscriber.jsx'
import {each, isEmpty, map, filter, cloneDeep, isObject} from 'lodash'
import {sortPrimitive, buildParamsFromEntity} from './utils.jsx'
import {displays} from '../widget/display.jsx'

export default class Table extends Component {
  componentWillReceiveProps(props) {
    let state = api.entities[props.params.entity]
    if (state) {
      state = cloneDeep(state)
      if (!isEmpty(props.params)) {
        if ('string' === typeof props.params.order) {
          props.params.order.split(/[., ]+/g).forEach(function (column) {
            if ('-' === column[0]) {
              state.fields[column.slice(1)].order = -1
            }
            else {
              state.fields[column].order = 1
            }
          })
        }
      }
      this.setState(state)
      this.source.params = {select: filter(state.fields, f => f.compact).map(f => f.name)}
      this.source.start()
    }
  }

  componentWillMount() {
    try {
      this.source = new Subscriber('lord/' + this.props.params.entity)
      this.source.subscribe(this)
      this.componentWillReceiveProps(this.props)
    }
    catch (ex) {
      console.error(ex)
    }
  }

  onReset(rows) {
    this.setState({rows})
  }

  render() {
    if (this.state) {
      const {rows, fields} = this.state
      let body
      if (rows) {
        body = rows.map(row => <Row key={row.id} {...{row, fields}}/>)
        body = <tbody>{body}</tbody>
      }
      else {
        body = <tbody/>
      }
      return <table className="table">
        <Header
          source={this.source}
          entity={this.state}
        />
        {body}
      </table>
    }
    else {
      return <div className="error">Entity {this.props.params.entity} not set</div>
    }
  }
}

export class Header extends Component {
  componentWillReceiveProps(props) {
    this.setState(props.entity)
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  onSort = (name, order) => {
    const fields = this.state.fields
    const field = fields[name]
    if (-1 === order) {
      field.order = 1
    }
    else if (1 === order) {
      field.order = 0
    }
    else {
      each(fields, (v, k) => {
        v.order = 0
        this.setState({[k]: v})
      })
      field.order = -1
    }
    this.setState({fields})
    this.props.source.change(buildParamsFromEntity(this.state))
  }

  render() {
    const columns = []
    each(this.state.fields, ({name, order, compact}) => {
      if (compact) {
        let className = 'fa fa-sort'
        if (order) {
          className += order > 0 ? '-asc' : '-desc'
        }
        columns.push(
          <th key={name} onClick={() => this.onSort(name, order)}>
            <i className={className}/>{name}
          </th>
        )
      }
    })
    return <thead>
    <tr>{columns}</tr>
    </thead>
  }
}

export const Row = ({row, fields}) => {
  const cells = map(row, (value, k) => {
    const field = fields[k]
    const display = displays[field.display]
    if (display) {
      value = display({value})
    }
    return value ? <td key={k}>{value}</td> : <td key={k}/>
  })
  return <tr>{cells}</tr>
}
