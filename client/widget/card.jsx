import React, {Component} from 'react'
import {each} from 'lodash'
import api from '../connect/api.jsx'

export class Social extends Component {
  render() {
    const buttons = []
    each(api.config.socials, (pattern, name) => {
      let url = this.props[name]
      if (url) {
        if (url.indexOf('http') !== 0) {
          url = pattern.replace('{id}', url)
        }
        buttons.push(<a
          key={name}
          className={'social ' + name}
          href={url}
          target="_blank"
        />)
      }
    })
    let className = 'social-networks widget'
    if (0 === buttons.length) {
      className += ' empty'
    }
    return <div className={className}>{buttons}</div>
  }
}

export function vkontakteURL(id) {
  return 'https://vk.com/id' + id
}

export class Contacts extends Component {
  render() {
    const skype = this.props.skype
      ? <div className="skype">{this.props.skype}</div>
      : ''
    const email = this.props.email
      ? <div className="email">{this.props.email}</div>
      : ''
    let link = ''
    if (this.props.link) {
      let text = this.props.link
      if (text.length > 48) {
        text = /^https?:\/\/([^\/]+)/.exec(text)
        text = text ? text[1] : this.props.link
      }
      link = <a className="link" href={this.props.link} target="_blank">{text}</a>
    }
    return <div className="contacts widget">
      {skype}
      {email}
      {link}
    </div>
  }
}

export class Vip extends Component {
  render() {
    return this.props.is_active || (this.props.active && new Date(this.props.active).getTime() > Date.now())
      ? <span className="vip-icon vip-active"/>
      : <span className="vip-icon vip-notactive"/>
  }
}

export class Name extends Component {
  render() {
    return <a href={vkontakteURL(this.props.id)} target="_blank" className="name widget">
      <span className="first">{this.props.forename}</span>
      &nbsp;
      <span className="last">{this.props.surname}</span>
      &nbsp;
      <Vip {...this.props}/>
    </a>
  }
}

export class Avatar extends Component {
  render() {
    const name = this.props.forename + ' ' + this.props.surname
    return <a href={vkontakteURL(this.props.id)} target="_blank" className="avatar-container widget">
      <div className="avatar" title={name}
           style={{backgroundImage: `url("${this.props.avatar}")`}}/>
    </a>
  }
}

export class About extends Component {
  render() {
    if ('string' === typeof this.props.about) {
      const about = this.props.about.split(/\s*\n\s*/g).map((p, i) => <p key={i}>{p}</p>)
      return <div className="user-about widget">{about}</div>
    }
    return <div className="user-about widget empty"/>
  }
}

export default class Card extends Component {
  render() {
    return <div className="card widget">
      <Avatar {...this.props}/>
      <div className="card-right">
        <Name {...this.props}/>
        <Contacts {...this.props}/>
        <About {...this.props}/>
        <Social {...this.props}/>
      </div>
    </div>
  }
}
