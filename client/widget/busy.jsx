import React, {Component} from 'react'

const Busy = ({children}) => <div className="busy">
  <img src="/images/busy.svg"/>
  {children}
</div>

export default Busy
