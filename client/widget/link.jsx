import React, {Component} from 'react'
import {isClient, announce} from '../globals'

export default function importLink(href, rel = 'stylesheet') {
  if (!/\.\w{2,3}$/.test(href)) {
    throw new Error(`importLink href ${href} must contain a dot`)
  }
  // if (isClient && localStorage.getItem('dom_link')) {
  //   {/*return <link href={href} rel={rel}/>*/}
  // }
  // else {
  //   require(__dirname + '/../public' + href)
    return <div className="import" style={{display: 'none'}}/>
  // }
}

announce({importLink})
