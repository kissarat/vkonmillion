import React, {Component} from 'react'
import {map, each, every, cloneDeep, extend, last, slice, create, isObject, filter} from 'lodash'

export const Image = ({value}) => <div className="display image" style={{backgroundImage: `url("${value}")`}}/>
export const Time = ({value, time}) => {
  value = new Date(value)
  value = time ? value.toLocaleString() : value.toLocaleDateString()
  return <div
    className={'display ' + (time ? 'time' : 'date')}
    style={{backgroundImage: `url("${value}")`}}>{value}</div>
}

export const displays = {
  image: Image,
  date: Time,
  time: ({value}) => Time({value, time: true})
}
