import {Router, Route, browserHistory} from 'react-router'
import React, {Component} from 'react'
import {AppRegistry, ScrollView, Image, Text} from 'react-native-web'
import {range} from 'lodash'

const List = () => {
  const list = range(30).map(text => <Text key={text}>{text}</Text>)
  return <ScrollView>{list}</ScrollView>
}

const route = <Route path="lab/">
  <Route path="list" component={List}/>
</Route>


export default route
