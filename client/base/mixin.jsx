import {each} from 'lodash'

export default function mixin(parent, child, statics) {
  const constructor = child.constructor
    ? child.constructor
    : parent ? parent : function Mixin() {

  }
  if (parent) {
    constructor.prototype = Object.create(parent.prototype)
    each(child, function (method, name) {
      constructor.prototype[name] = method
    })
  }
  else {
    constructor.prototype = child
  }
  constructor.mix = function (target, ...args) {
    if (parent && parent.mix) {
      parent.mix(target)
    }
    Object.assign(target, constructor.prototype)
    constructor.apply(target, args)
  }
  each(statics, function (method, name) {
    constructor[name] = method
  })
  return constructor
}