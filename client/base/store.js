const {isObject} = require('lodash')

function promisable(result, cb) {
  if (isObject(result) && result.then instanceof Function) {
    return result.then(cb)
      .catch(function (err) {
        console.error(err)
      })
  }
  else {
    return cb(result)
  }
}

class Store {
  constructor(reducer, action = {}) {
    this.reducer = reducer
    this.listeners = []
    this.setState(reducer(action))
  }

  setState(state) {
    this.state = state
  }

  getState() {
    return this.state
  }

  dispatch(action) {
    promisable(this.getState(), (oldState)
      => promisable(this.reducer(oldState, action), (newState)
      => promisable(this.setState(newState), () => this.listeners.forEach(l => l(newState, action)))))
  }

  replaceReducer(nextReducer) {
    this.reducer = nextReducer
  }

  subscribe(listener) {
    this.listeners.push(listener)
    if (this.listeners.length > Store.maxListeners) {
      console.warn(`subscribe: ${this.listeners.length} > ${Store.maxListeners}`, listener)
    }
  }

  unsubscribe(listener) {
    this.listeners = this.listeners.filter(l => l !== listener)
    if (this.listeners.length > Store.maxListeners) {
      console.warn(`unsubscribe: ${this.listeners.length} > ${Store.maxListeners}`, listener)
    }
  }
}

Store.maxListeners = 10

Store.createStore = function createStore(reducer) {
  return new Store(reducer)
}

class LocalStore extends Store {
  constructor(id, reducer, action = {}) {
    super(reducer, action)
    this.id = id
  }

  setState(state) {
    localStorage.setItem(this.id, JSON.stringify(state))
  }
}

LocalStore.cid = 'rx'
LocalStore.nextId = function nextId() {
  if (!LocalStore.lastId) {
    const ids = Object.keys(localStorage)
      .filter(key => key.indexOf(LocalStore.cid) === 0)
      .map(key => parseInt(key.slice(LocalStore.cid.length)))
    LocalStore.lastId = ids.reduce(Math.max, 0)
  }
  return ++LocalStore.lastId
}

LocalStore.createStore = function createStore(reducer) {
  return new LocalStore(LocalStore.nextId(), reducer)
}

module.exports = {
  Store,
  LocalStore,
  promisable
}
