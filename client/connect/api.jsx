import 'whatwg-fetch'
import {isEmpty, each} from 'lodash'
import {announce} from '../globals'
import qs from '../base/urlencoded.jsx'

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  Author: 'Taras Labiak <kissarat@gmail.com>'
}

class API {
  constructor() {
    this.prefix = '/serve'
  }

  error(err) {
    console.error(err)
  }

  getToken() {
    const match = /vk=([^;]+)/.exec(document.cookie)
    return match ? match[1] : ''
  }

  setToken(value, expires) {
    expires = +expires
    const token = `vk=${value}; path=/; max-age=` + (expires || 3600 * 24 * 365)
    console.log(token)
    document.cookie = token
  }

  setTokenFromQuery() {
    const query = qs.parse(location.search.slice(1))
    if (query.access_token) {
      localStorage.setItem('user_id', query.user_id)
      this.setToken(query.access_token, query.expires_in * 1000)
      return true
    }
    return false
  }

  buildURL(url, params) {
    if (/^\w+\/\w+$/.test(url)) {
      const token = this.getToken()
      if (token) {
        url = `/~${token}/${url}`
      }
      else {
        url = '/' + url
      }
    }
    if (!isEmpty(params)) {
      url += '?' + qs.stringify(params)
    }
    return url
  }

  get(url, params) {
    url = this.prefix + this.buildURL(url, params)
    // console.log(url)
    return fetch(url, {headers}).then(r => r.json())
  }

  send(url, params, data) {
    if (!data) {
      data = params
      params = null
    }
    url = this.buildURL(url, params)
    const options = {
      method: 'POST',
      headers
    }
    if (!isEmpty(data) && true !== data) {
      options.body = JSON.stringify(data)
    }
    return fetch(this.prefix + url, options)
      .then(function (r) {
        const json = r.json()
        if (json.error) {
          this.error(json.error)
        }
        return json
      })
  }

  get entities() {
    return this.config.entities
  }
}

const api = new API()
announce({api})
module.exports = api

// window.api = module.exports
