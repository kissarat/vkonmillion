import React, {Component} from 'react'
import alertList from '../widget/alert.jsx'
import Menu from '../widget/menu.jsx'

const menu = [
  {
    name: 'Пользователи',
    url: '/table/user',
    icon: 'fa fa-users'
  },
  {
    name: 'Платежы',
    url: '/table/transfer',
    icon: 'fa fa-usd'
  },
  {
    name: 'Реферальные переходы',
    url: '/table/goref',
    icon: 'fa fa-sign-in'
  }
]

export default class AdminApp extends Component {
  render() {
    return <div id="admin">
      <header>
        <Menu items={menu}/>
      </header>
      <div id="main">
        <main>{this.props.children}</main>
      </div>
    </div>
  }
}
