import React, {Component} from 'react'
import Menu from '../widget/menu.jsx'
import importLink from '../widget/link.jsx'

const styleLink = importLink('/style.css')

const menu = [
  ['Главная', '/index.html'],
  ['Маркетинг', '/marketing.html'],
  ['О проекте', '/o-proekte.html'],
  ['Промо', '/promo.html'],
  ['Кабинет', '/enter']
]
  .map(([name, url]) => ({name, url, external: true}))

export class Header extends Component {
  render() {
    return <header>
      {styleLink}
      <div className="header-container">
        <img className="logo" src="/images/logo-text.png"/>
        <nav>
          <Menu items={menu}/>
        </nav>
      </div>
    </header>
  }
}

export class Outdoor extends Component {
  render() {
    return <div className="outdoor layout">
      <Header/>
      <div className="container">{this.props.children}</div>
      <footer>
        <div className="footer-container">
          <a className="payeer" href="https://payeer.com/?partner=32316">
            <img src="/images/payeer-logo.jpg"/>
          </a>
        </div>
      </footer>
    </div>
  }
}
