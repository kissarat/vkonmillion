import React, {Component} from 'react'
import {browserHistory} from 'react-router'
import api from '../connect/api.jsx'
import {map, size, sample, first} from 'lodash'
import Card, {vkontakteURL} from '../widget/card.jsx'
import Busy from '../widget/busy.jsx'

function getCandidates({user_ids}) {
  const f = () => api.get('vkontakte/candidates', {user_id: localStorage.getItem('user_id')})
  if (user_ids) {
    return api
      .send('vkontakte/updateStatus', {status: 'subscribed'}, {user_ids})
      .then(f)
  }
  return f()
}

function onError(err) {
  console.error(err)
}

export class CandidateList extends Component {
  componentWillReceiveProps(props) {
    this.setState({busy: true})
    api.setTokenFromQuery()
    getCandidates(props)
      .then((state) => {
        state.busy = false
        this.setState(state)
      })
      .catch(onError)
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
    VK.Observer.subscribe('widgets.subscribed', (n) => {
      if (n >= api.config.vkontakte.subscribers) {
        api.send('vkontakte/updateStatus',
          {status: 'subscribed'},
          {user_ids: map(this.state.candidates, c => c.id)})
          .then(function ({status}) {
            console.log(status)
            if ('subscribed' === status) {
              browserHistory.push('/share')
            }
          })
      }
    })
  }

  refButton = (el) => {
    if (el) {
      VK.Widgets.Subscribe(el.id, {soft: 0}, +el.id)
    }
  }

  render() {
    const candidates = size(this.state.candidates)
      ? map(this.state.candidates, ({id, avatar, surname, forename}) => <div key={id}>
      <div id={id} className="subscribe" ref={this.refButton}></div>
    </div>)
      : <div className="alert alert-danger">
      Нету свободных кандидатов для отправки заявок в друзья.
      Обратитесь в <a href={api.config.feedback}>службу поддерки</a> указав
      ваш ID ВКонтакте {localStorage.getItem('user_id')}
    </div>
    return <div className="step-2 candidates">
      <h1>Шаг №2</h1>
      <h2>Активируйте платформу</h2>
      <h3>Для активации добавьте в друзья учасников платформы</h3>
      <div className="list">{candidates}</div>
    </div>
  }
}

export class Follow extends Component {
  componentWillReceiveProps(props) {
    this.setState({
      busy: true,
      current: {}
    })
    if (api.setTokenFromQuery()) {
      browserHistory.push('/follow')
    }
    getCandidates(props)
      .then((state) => {
        state.busy = false
        state.current = first(state.candidates)
        if (!state.need && props && props.user_ids) {
          browserHistory.push('/share')
        }
        else {
          this.setState(state)
        }
      })
      .catch(onError)
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  getName() {
    return `${this.state.current.forename} ${this.state.current.surname}`
  }

  onClick = (e) => {
    e.preventDefault()
    this.setState({busy: true})
    const w = open(vkontakteURL(this.state.current.id), this.getName(),
      'left=200,top=200,width=800,height=800,toolbar=0,resizable=0')
    const timer = setInterval(() => {
      if (w.closed) {
        clearInterval(timer)
        this.componentWillReceiveProps({user_ids: map(this.state.candidates, c => c.id)})
      }
    }, 300)
  }

  onClickSkip = () => {
    this.componentWillReceiveProps({})
  }

  render() {
    if (this.state.busy) {
      return <Busy>Загрузка...</Busy>
    }
    else {
      const addButton = !this.state.current
        ? <Busy/>
        : [<div key="add"
                className="btn btn-primary"
                id={this.state.current.id}
                onClick={this.onClick}>
        Добавить в друзья
      </div>]
      if (addButton instanceof Array && this.state.error) {
        addButton.push(<span
          key="next"
          className="btn btn-warning"
          onClick={this.onClickSkip}>
          Пропустить
        </span>)
      }
      return <div className="step-2 candidate">
        <h1>Шаг №2</h1>
        <h2>Активируйте платформу</h2>
        <h3>Для активации добавьте в друзья учасников платформы</h3>
        <div className="count">
          Добавлено <span>{this.state.requests.length}</span>
          <span> из </span>
          <span>{this.state.requests.length + this.state.enough}</span>
        </div>
        <Card {...this.state.current}/>
        {addButton}
      </div>
    }
  }
}
