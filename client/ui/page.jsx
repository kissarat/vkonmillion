import React, {Component} from 'react'
import importLink from '../widget/link.jsx'

const importStyle = importLink('/style.css')

export class NotFound extends Component {
  render() {
    return <div className="page error not-found">
      {importStyle}
      <img
        src="/images/not-found.png"
        alt="Случилась ошибка, попробуйте загрузить страницу позже"
      />
    </div>
  }
}

export class Unavailable extends Component {
  render() {
    return <div className="page error unavailable">
      {importStyle}
      <img
        src="/images/unavailable.jpg"
        alt="Страница не найдена"
      />
    </div>
  }
}

export class Development extends Component {
  render() {
    return () => <div className="page error development">
      {importStyle}
      <img
        src="/images/development.png"
        alt="Страница не найдена"
      />
    </div>
  }
}
