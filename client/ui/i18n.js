const dictionary = {
  'An error occurred while getting user information': 'Во время получения информации о пользователе произошла ошибка',
  'You have insufficient funds': 'У вас недостаточно средств',
  'An error occurred while getting a sponsor': 'Во время получения спонсора произошла ошибка',
  'You do not have a sponsor': 'У вас нет спонсора',
  'Your sponsor id{id} is not active': 'Ваш спонсор id{id} не активен',
  'An error occurred while obtaining the list of sponsors': 'Произошла ошибка при получении списка спонсоров',
  'Your sponsor have no sponsor': 'У вашего спонсора нет спонсора',
  'An error occurred when obtaining a list of your sponsor referrals': 'Во время получения списка реферала вашего спонсора произошла ошибка'
}

module.exports = function T(m, vars) {
  return m.replace(/\{[^}]+}/g, function (match) {
    return vars[match.slice(1, -1)]
  })
}
