import React, {Component} from 'react'
import api from '../connect/api.jsx'
import Menu from '../widget/menu.jsx'

export default class Admin extends Component {
  render() {
    const items = [
      ['Пользователи', '/serve' + api.buildURL('user/all')],
      ['Граф', '/graph.html'],
      ['Платежы Payeer', '/serve' + api.buildURL('payeer/history')],
      ['Баланс Payeer', '/serve' + api.buildURL('payeer/balance')],
      ['Платежы', '/serve' + api.buildURL('transfer/index')],
      ['Заблокированые', '/serve' + api.buildURL('user/all', {blocked: 1})],
      ['Администраторы', '/serve' + api.buildURL('user/all', {admin: 1})],
      ['Последние регистрации', '/serve' + api.buildURL('user/last')],
      ['API', '/serve' + api.buildURL('/modules')],
      ['Конфигурация', '/serve' + api.buildURL('/config')],
    ]
    return <div className="page admin">
      <Menu items={Menu.normalize(items, '_blank')}/>
    </div>
  }
}
