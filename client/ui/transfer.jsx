import React, {Component} from 'react'
import {Link, browserHistory} from 'react-router'
import api from '../connect/api.jsx'
import {omit, each, cloneDeep} from 'lodash'
import Busy from '../widget/busy.jsx'

const fields = {
  id: {
    label: 'Время',
    readOnly: true,
    output(value) {
      return new Date(value / (1000 * 1000)).toLocaleString()
    }
  },

  amount: {
    label: 'Сумма'
  },

  order_id: {
    label: 'ID',
    readOnly: true
  }
}

export class Success extends Component {
  componentWillReceiveProps(props) {
    api.send('payeer/chess', {transaction: props.params.id}).then(function (r) {
      const array = [];
      ['message', 'vars', 'error'].forEach(function (name) {
        if (r[name]) {
          array.push(r[name])
        }
      })
      browserHistory.push(401 === r.statusCode
        ? '/login'
        : `/alert/${r.type}/` + btoa(JSON.stringify(array))
      )
    })
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    //return <Busy>Подождите, идет осуществления выплат...</Busy>

    return <div className="page success viewport">
      <Busy>
        <div className="warning">Подождите, идет осуществления выплат...</div>
        <div className="attention">Не закрывайте страницу!</div>
      </Busy>
    </div>
  }
}

const types = {
  payment: 'Оплата',
  accrue: 'Перевод',
  'write-off': 'Списание',
  support: 'Поддержка',
  withdraw: 'Поддержка',
}

const statuses = {
  success: 'Удачно',
  fail: 'Не удачно',
  created: 'Создано'
}

function round(value) {
  return (Math.ceil(value * 100) / 100).toFixed(2)
}

export class TransferList extends Component {
  componentWillMount() {
    this.setState({busy: true})
    api.get('transfer/index').then((list) => this.setState({busy: false, list}))
  }

  render() {
    let table
    if (this.state.list) {
      table = this.state.list.map(t => <tr key={t.id}>
        <td>{new Date(t.id / (1000 * 1000)).toLocaleString()}</td>
        <td className="money">{localStorage.getItem('user_id') == t.to ? +t.amount : -t.amount}</td>
        <td>{types[t.type]}</td>
        <td>{statuses[t.status]}</td>
        <td>{t.order_id || t.id}</td>
      </tr>)
      table = <table className="table">
        <thead>
        <tr>
          <td>Время</td>
          <td>Сумма</td>
          <td>Тип</td>
          <td>Статус</td>
          <td>ID</td>
        </tr>
        </thead>
        <tbody>{table}</tbody>
      </table>
    }
    else {
      table = <div className="busy">Загрузка...</div>
    }
    return <div className="page transfer">
      <h1>Финансы</h1>
      {table}
    </div>
  }
}

export class Vip extends Component {
  componentWillMount() {
    this.setState({})
    api.get('user/me').then((me) => this.setState(me))
  }

  render() {
    let realAmount = api.config.chess.in
      + api.config.chess.in * api.config.payeer.commission
    realAmount *= 1 + api.config.payeer.commission
    realAmount = round(realAmount)
    const payURL = `/serve/payeer/pay?user_id=${localStorage.getItem('user_id')}&amount=` + api.config.chess.in
    return <div className="cabinet-page vip">
      <h1>Шаг №4</h1>
      <h2>Активируйте VIP-платформу</h2>
      <div className="last-step">Остался последний шаг - оплата</div>
      <div className="use-payeer">
        Для оплаты и выплаты средств
        используется платежная система Payeer
      </div>
      <table>
        <caption>Стоимость VIP</caption>
        <tbody>
        <tr>
          <td>Отправитель</td>
          <td className="name">
            <span className="first">{this.state.forename}</span>
            &nbsp;
            <span className="last">{this.state.surname}</span>
          </td>
        </tr>
        <tr>
          <td>Получатель</td>
          <td>vk-mm.com</td>
        </tr>
        <tr>
          <td>Дата</td>
          <td>{new Date().toLocaleDateString()}</td>
        </tr>
        <tr>
          <td>Примечание</td>
          <td>Активация VIP</td>
        </tr>
        <tr>
          <td>Сумма</td>
          <td>
            <div className="value money">{round(api.config.chess.out)}</div>
          </td>
        </tr>
        <tr>
          <td>Обслуживание</td>
          <td>
            <div className="value money">{round(api.config.chess.in - api.config.chess.out)}</div>
          </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
          <td colSpan="2">
            <a href={payURL} className="btn btn-primary">Оплатить VIP</a>
            <span className="commission">
              С учетом комиссии Payeer - <span className="value money">{realAmount}</span>
            </span>
          </td>
        </tr>
        </tfoot>
      </table>
      <div className="wtf">
        <a target="_blank">Зачем покупать VIP?</a>
        <i>(открывается в новом окне)</i>
      </div>
      <Link className="back" to="/cabinet">Пропустить и перейти в личный кабинет</Link>
    </div>
  }
}
