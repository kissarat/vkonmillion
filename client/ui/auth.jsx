import React, {Component} from 'react'
import {browserHistory} from 'react-router'
import api from '../connect/api.jsx'

function loginURL(self, ref) {
  let url = api.prefix + '/vkontakte'
  if (ref) {
    localStorage.setItem('ref', ref)
  }
  ref = +localStorage.getItem('ref')
  if (ref) {
    url += '?ref=' + ref
  }
  if (!self.state || url !== self.state.url) {
    api.get('/user/get', {id: ref || api.config.root}).then((r) => {
      const state = r || {}
      r.busy = false
      self.setState(state)
    })
    if (ref && !+localStorage.getItem('goref')) {
      const params = {id: ref}
      if (localStorage.getItem('user_id')) {
        params.from = localStorage.getItem('user_id')
      }
      if (document.referrer) {
        params.referrer = document.referrer
      }
      api.send('/user/goref', params, true).then(function () {
        localStorage.setItem('goref', 1)
      })
    }
  }
  self.setState({
    url,
    busy: true
  })
}

export class Login extends Component {
  componentWillReceiveProps() {
    loginURL(this)
  }

  componentWillMount() {
    this.componentWillReceiveProps()
  }

  render() {
    return <div className="page login">
      <h1>Войти</h1>
      <h2>Войдите через <span>VK</span></h2>
      <a href={this.state.url}>Ввойти</a>
    </div>
  }
}

export class Signup extends Component {
  onClickResetReferral() {
    localStorage.removeItem('ref')
    browserHistory.push('/signup')
  }

  componentWillReceiveProps(props) {
    loginURL(this, props.params.id)
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    const {avatar, surname, forename} = this.state
    const referral = this.state.busy
      ? <div className="busy">Загрузка информации о спонсоре...</div>
      : <div className="referral">
      <div className="attention"><span>Убедитесь!</span> Что вас пригласил</div>
      <div className="avatar" style={{backgroundImage: `url("${avatar}")`}}/>
      <div className="name">
        <span className="first">{forename}</span>
        &nbsp;
        <span className="last">{surname}</span>
      </div>
      <div className="shit">Нет? Нажмите на кнопку ниже и перейдите еще раз по ссылке от спонсора</div>
      <button type="button" className="no" onClick={this.onClickResetReferral}>Нет!</button>
    </div>
    return <div className="page step-1 signup">
      <h1>Шаг 1</h1>
      <div className="purpose">Зарегистрирутесь через <span>VK</span></div>
      <a href={this.state.url}>VK Регистрация</a>
      {referral}
    </div>
  }
}

export class Logout extends Component {
  componentWillMount() {
    api.setToken('', 0)
    location.pathname = '/login'
  }

  render() {
    return <div className="busy">
      Выход...
    </div>
  }
}

export class Enter extends Component {
  componentWillMount() {
    const params = {}
    const user_id = localStorage.getItem('user_id')

    if (user_id) {
      params.user_id = user_id
    }
    api.get('home/enter', params).then(function (r) {
      if (r.found) {
        browserHistory.push(r.authenticated ? '/cabinet' : '/login')
      }
      else {
        browserHistory.push('/signup')
      }
    })
  }

  render() {
    return <div className="enter">Загрузка...</div>
  }
}
