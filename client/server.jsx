global.XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest
global.fetch = require('whatwg-fetch').fetch
import React, {Component} from 'react'
const {renderToStaticMarkup, renderToString} = require('react-dom/server')
import {match, RouterContext} from 'react-router'
import {routes} from './main.jsx'
import api from './connect/api.jsx'
api.config = require('../config')
import {readFileSync} from 'fs'
import {createServer} from 'http'
import salo from 'salo'

let template = readFileSync(__dirname + '/public/react.html').toString()
api.prefix = api.config.origin + api.prefix

createServer(function (req, res) {
  match({routes, location: req.url}, function (error, redirectLocation, renderProps) {
    // console.log(error, redirectLocation, renderProps)
    if (error) {
      salo(res)(err)
    }
    else if (redirectLocation) {
      res.writeHead(302, {
        location: redirectLocation.pathname + redirectLocation.search
      })
      res.end()
    }
    else if (renderProps) {
      let s = renderToStaticMarkup(<RouterContext {...renderProps}/>)
      //err = JSON.stringify(salo(err), null, '\t')
      //s = `<template id="server-render-error">\n${err}\n</template>\n${s}`
      const page = template
        .replace('<meta charset="UTF-8">', `<meta charset="UTF-8"><base href="${api.config.origin + req.url}"/>`)
        .replace('<div id="app"></div>', `<div id="app">${s}</div>`)
        .replace('<script src="/gay.js"></script>', '')
        // .replace(/\s*<script([^<\n]+)<\/script>\s*/g, '')
      res.end(page)
    }
    else {
      res.statusCode = 404
      res.end()
    }
  })
})
  .listen(8002)
