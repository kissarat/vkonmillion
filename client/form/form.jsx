import React, {Component} from 'react'
import {each, isEmpty, map, pick} from 'lodash'
import {fieldList} from './field.jsx'
import api from '../connect/api.jsx'
import {ButtonToolbar, Button} from 'react-bootstrap'

export default class Form extends Component {
  componentWillReceiveProps() {
    this.setState({})
  }

  componentWillMount() {
    this.componentWillReceiveProps()
  }

  change = (value, name) => {
    this.setState({[name]: value})
  }

  onSubmit = (e) => {
    e.preventDefault()
    const changes = this.state
    if (this.props.submit instanceof Function) {
      this.props.submit(changes)
    }
    else if (isEmpty(changes)) {
      this.props.onSuccessSubmit(changes)
    }
    else {
      const promise = isEmpty(this.props.params)
        ? api.send(this.props.action, changes)
        : api.send(this.props.action, this.props.params, changes)
      if (this.props.onSuccessSubmit instanceof Function) {
        promise.then((r) => {
          this.props.onSuccessSubmit(changes, r)
        })
      }
    }
  }

  render() {
    const title = this.props.title || this.props.name
    const attributes = pick(this.props.form, 'method', 'action')
    const button = this.props.readOnly ? '' :
      <ButtonToolbar>
        <Button bsStyle="success" type="submit">{this.props.button || title}</Button>
      </ButtonToolbar>
    return <form {...attributes} onSubmit={this.onSubmit}>
      <h1>{title}</h1>
      {fieldList({fields: this.props.fields, change: this.change})}
      {button}
    </form>
  }
}
