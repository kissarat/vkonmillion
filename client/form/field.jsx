import React, {Component} from 'react'
import {each, isEmpty, map} from 'lodash'
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap'

export const Select = ({items, name, value, change}) => {
  const options = map(items, (label, value) => <option
    key={value}
    value={value}>
    {label}
  </option>)
  if (!value) {
    options.unshift(<option/>)
  }
  return <select
    name={name}
    value={value}
    onChange={e => change(e.target.value, e.target.name)}>
    {options}
  </select>
}

export default class Field extends Component {
  componentWillReceiveProps(props) {
    this.setState({})
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  onChange = (e) => {
    let value = e.target.value
    if ('function' === typeof this.props.input) {
      value = this.props.input(value)
    }
    value = isEmpty(value) ? null : value
    this.setState({value, changed: this.props.value != value})
    if (this.props.change instanceof Function) {
      this.props.change(value, this.props.name)
    }
  }

  render() {
    const attributes = {};
    function assign(obj) {
      each(obj, (v, k) => {
        if (v && ['name', 'value', 'maxLength', 'rows', 'patten'].indexOf(k) >= 0) {
          attributes[k] = v
        }
      })
    }
    assign(this.props)
    assign(this.state)
    if (!attributes.value) {
      attributes.value = ''
    }
    if ('hidden' === attributes.type) {
      return <input {...attributes}/>
    }
    else {
      let control
      if (this.props.items) {
        control = <Select {...attributes} change={this.props.change}/>
      }
      else {
        attributes.onChange = this.onChange
        attributes.className = 'form-control'
        // debug(attributes)
        control = React.createElement(attributes.rows ? 'textarea' : 'input', attributes)
      }
      return <FormGroup>
        <ControlLabel>{this.props.label || this.props.name}</ControlLabel>
        {control}
      </FormGroup>
    }
  }
}

export const fieldList = ({fields, change}) => map(fields, (field, name) => {
  if (!field.name) {
    field.name = name
  }
  return <Field
    change={change}
    key={name}
    {...field}
  />
})
