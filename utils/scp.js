const client = require('scp2')
const {extend} = require('lodash')

function SecureCopy(options) {
  extend(this, options)
}

SecureCopy.prototype = {
  apply(compiler) {
    compiler.plugin('done', () => this.upload())
  },

  upload() {
    client.scp(this.file, this, function (err) {
      if (err) {
        console.error(err)
      }
    })
  }
}

module.exports = SecureCopy
