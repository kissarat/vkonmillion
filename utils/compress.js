const lzma = require('lzma')
const fs = require('fs')

function Compress(options) {
  Object.assign(this, options)
}

Compress.prototype = {
  apply(compiler) {
    compiler.plugin('compilation', () => this.compress())
  },

  compress() {
    lzma.compress(fs.readdirSync(this.from), function (result, err) {
      if (err) {
        console.error(err)
      }
      else {
        fs.writeFileSync(this.to, result)
      }
    })
  }
}

module.exports = Compress
