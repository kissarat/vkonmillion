var request = require('request');
const {stringify} = require('querystring')
const {payeer} = require('../../config')
const {last} = require('lodash')

const amount = +last(process.argv)
let commission = amount * payeer.commission
// commission *= 1 - payeer.commission

let to = process.argv[process.argv.length - 2]
if (!/^P\d+/.test(to)) {
  to = 'P48798914'
}
console.log(to, amount)

const body = stringify({
  account: payeer.account,
  apiId: payeer.id,
  apiPass: payeer.password,
  curOut: 'RUB',
  curIn: 'RUB',
  action: 'transfer',
  sum: amount + commission,
  to
})

request({
  method: 'POST',
  url: 'https://payeer.com/ajax/api/api.php',
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  body
}, function (error, response, body) {
  const cookie = response.headers['set-cookie']
  if (cookie instanceof Array) {
    cookie.forEach(function (cookie) {
      console.log(/PHPSESSID=([^;]+)/.exec(cookie)[1])
    })
  }
  console.log(body);
});
