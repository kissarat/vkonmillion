var request = require('request');
const {stringify} = require('querystring')
const {payeer} = require('../../config')

const body = stringify({
  account: payeer.account,
  apiId: payeer.id,
  apiPass: payeer.password,
  action: 'history'
})

request({
  method: 'POST',
  url: 'https://payeer.com/ajax/api/api.php',
  headers: {
    accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  body
}, function (error, response, body) {
  const cookie = response.headers['set-cookie']
  if (cookie instanceof Array) {
    // cookie.forEach(function (cookie) {
    //   console.log(/PHPSESSID=([^;]+)/.exec(cookie)[1])
    // })
  }
  console.log(JSON.parse(body));
});
