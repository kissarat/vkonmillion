const merge = require('deepmerge')
const fs = require('fs-extra')
const {resolve} = require('path')
const argv = require('optimist')
  .default('settings', './local')
  .argv

var local
try {
  local = require(argv.settings)
}
catch (ex) {
  console.warn('No local config')
}

const config = {
  mode: process.env.VKMM || 'dev',
  prefix: '/serve',
  static: {dir: __dirname + '/client/public'},
  payeer: {
    ips: ['185.71.65.92', '185.71.65.189', '149.202.17.210'],
    commission: 0.01
  },
  chess: {
    in: 600,
    out: 500
  },
  vkontakte: {
    v: 5.59
  },
  http: {
    hostname: "0.0.0.0",
    port: 8001,
    log: {
      enabled: true,
      methods: ['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT'],
      exclude: {
        headers: ['host', 'connection', 'accept-language', 'accept-encoding', 'author', 'pragma',
          'cache-control', 'no-cache', 'upgrade-insecure-requests', 'origin'],
        urls: [
          '/config',
          /^\/~\w+\/user\/goref/,
          /^\/~\w+\/user\/last/,
          /^\/~\w+\/user\/me/,
        ]
      },
      limits: {
        data: 65536
      }
    }
  },

  socials: {
    // skype: 'skype:{id}?chat',
    facebook: 'https://facebook.com/{id}',
    odnoklassniki: 'https://ok.ru/profile/{id}',
    twitter: 'https://twitter.com/{id}'
  }
}

const settings = local ? merge(config, local) : config

function autosave() {
  function extract(node, local) {
    const result = {}
    for (const key in local) {
      const value = local[key]
      result[key] = value && 'object' === typeof value ? extract(node[key], value) : value
    }
    return result
  }

  fs.writeJson(argv.settings, extract(settings, local), function (err) {
    if (err) {
      console.error(err)
    }
  })
}

if (settings.autosave) {
  argv.settings = resolve(__dirname + '/' + argv.settings + '.json')
  console.log(`Automatic save local configuration to ${argv.settings} every ${config.autosave} seconds`)
  setInterval(autosave, config.autosave * 1000)
}

module.exports = settings

if (!module.parent) {
  const cat = process.argv[process.argv.length - 1]
  console.log(JSON.stringify(cat ? settings[cat] : settings))
}
