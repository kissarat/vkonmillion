const request = require('request-promise')
const {stringify} = require('querystring')
const {sample} = require('lodash')
const config = require('../config')
const db = require('schema-db')
// const vkontakte = require('../server/modules/vkontakte')

let access_token

function error(err) {
  console.error(err)
  process.exit(1)
}

function usersGet(ids) {
  const params = stringify({
    access_token,
    user_ids: ids.join(',')
  })
  return request('https://api.vk.com/method/users.get?' + params)
}

db.setup(config.database)
  .then(function () {
    return db.entities.user_token.read({blocked: false, order: '-expires'})
  })
  .then(function (user_tokens) {
    access_token = sample(user_tokens).token
    return usersGet(user_tokens.map(t => t.id))
  })
  .then(function (body) {
    body = JSON.parse(body)
    if (body.response instanceof Array) {
      const blocked = body.response
        .filter(u => u.deactivated)
        .map(u => u.id || u.uid)
      if (blocked.length > 0) {
        return db.table('user')
          .where('id', 'in', blocked)
          .update({blocked: true})
      }
    }
    else {
      error(body)
    }
  })
  .then(function () {
    return db.entities.user_token.read({blocked: true, order: '-expires'})
  })
  .then(function (user_tokens) {
    return usersGet(user_tokens.map(t => t.id))
  })
  .then(function (body) {
    body = JSON.parse(body)
    if (body.response instanceof Array) {
      const active = body.response
        .filter(u => !u.deactivated)
        .map(u => u.id || u.uid)
      if (active.length > 0) {
        return db.table('user')
          .where('id', 'in', active)
          .update({blocked: false})
      }
    }
    else {
      error(body)
    }
  })
  .then(function () {
    process.exit()
  })
  .catch(error)
