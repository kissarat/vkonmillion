const config = require('../config')
const express = require('express')
const modules = require('./modules')
const db = require('schema-db')
const fs = require('fs-extra')
const {createServer} = require('http')
const {route, reflectionRoute, errorRoute, elseRoute, r} = require('./helpers')
const {keyBy} = require('lodash')
require('./utils')

db.setup(config.database)
  .then(function () {
    return db.table('config').orderBy('name')
      .then(rows => keyBy(rows, 'name'))
  })
  .then(function (c) {
    require('./entities')
    config.chess.in = +c.chess_in.float
    config.chess.out = +c.chess_out.float
    // fs.outputJsonSync(__dirname + '/../client/public/config.json', modules.home.config())
    const app = express()

    app.use(require('./log'))
    app.use(require('./auth'))

    app.get('/modules', reflectionRoute)
    app.get('/favicon.ico', (req, res) => res.redirect('https://help.ubuntu.com/favicon.ico'))
    app.get('/get', r(modules.user.get))
    app.get('/config', r(modules.home.config))
    app.get('/vkontakte/describe', r(modules.vkontakte.describe))
    app.get('/vkontakte', modules.vkontakte.authorize)
    app.get('/vkontakte/user', r(modules.vkontakte.user))
    app.get('/payeer/pay', modules.payeer.pay)
    app.all('/payeer/success', modules.payeer.status)
    app.all('/payeer/fail', modules.payeer.status)
    app.all('/payeer/status', modules.payeer.status)
    app.get('/payeer/chess', modules.payeer.chess)

    app.use(function (req, res, next) {
      if (req.user && req.user.admin) {
        db.lord(req, res, next)
      }
      else {
        next()
      }
    })
    app.use(route)
    app.use(errorRoute)
    app.use(elseRoute)

    createServer(app).listen(
      config.http.port,
      config.http.hostname
    )
  })
