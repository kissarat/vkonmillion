const {each, isObject} = require('lodash')
const db = require('schema-db')

const regex = {
  email: /^[\w\-.]+@[\w\-.]+\.\w+$/i,
  url: /^https?..\a[\w\-]+\/[\w\-\/._]+$/i,
  phone: /^\+?[0-9]{3}-?[0-9]{6,12}$/
}

function assign(target, source) {
  for (const key in source) {
    const sv = source[key]
    if (isObject(sv) && !(sv instanceof RegExp)) {
      if (!isObject(target[key])) {
        target[key] = {}
      }
      assign(target[key], sv)
    }
    else {
      target[key] = sv
    }
  }
}

assign(db.entities, {
  user: {
    fields: {
      avatar: {
        display: 'image',
        pattern: regex.url
      },
      birthday: {
        display: 'date'
      },
      created: {
        display: 'time'
      },
      email: {
        type: 'email',
        pattern: regex.email
      },
      payeer: {
        required: true,
        pattern: /^P\d{8}$/
      }
    }
  },

  transfer: {
    fields: {
      id: {
        name: 'Time',
        display: 'time'
      },

      order_id: {
        name: 'ID'
      }
    }
  }
})

const tables = {
  user: ['id', 'forename', 'surname', 'avatar', 'payeer', 'created'],
  transfer: ['id', 'amount', 'type', 'status', 'order_id']
}

each(tables, function (columns, name) {
  const entity = db.entities[name]
  if (entity) {
    columns.forEach(function (column) {
      const field = entity.fields[column]
      if (field) {
        field.compact = true

        if (field.pattern && 'string' !== typeof field.pattern) {
          field.pattern = field.pattern.toString()
        }
      }
      else {
        console.error(`Column "${name}"."${column}" not found`)
      }
    })
  }
  else {
    console.error(`Entity "${name}" not found`)
  }
})
