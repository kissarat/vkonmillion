const config = require('../../config')
const db = require('schema-db')

module.exports = {
  config() {
    return {
      feedback: config.feedback,
      chess: config.chess,
      root: config.root,
      vkontakte: {
        id: config.vkontakte.auth.client_id,
        subscribers: config.vkontakte.subscribers
      },
      payeer: {
        commission: config.payeer.commission
      },
      entities: db.entities,
      socials: config.socials
    }
  },

  enter({user_id}) {
    if (this.user) {
      return {
        found: true,
        authenticated: true
      }
    }
    else if (user_id) {
      return db.entities.user.findOne({id: user_id}).then((user) => user
        ? {statusCode: 401, found: true}
        : {statusCode: 404, found: false}
      )
    }
    else {
      return {statusCode: 404}
    }
  }
}
