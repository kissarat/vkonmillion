const merge = require('deepmerge')
const request = require('request-promise')
const salo = require('salo')
const db = require('schema-db')
const {pick, each, keyBy, slice} = require('lodash')
const {stringify} = require('querystring')
const {vkontakte, root} = require('../../config')

const vk = {
  auth: vkontakte.auth,
  permissions: {
    //ads: 'Доступ к расширенным методам работы с <a href="/pages?oid=-1&amp;p=Ads_API">рекламным API</a>.',
    audio: 'Доступ к аудиозаписям.',
    docs: 'Доступ к документам.',
    friends: 'Доступ к друзьям.',
    groups: 'Доступ к группам пользователя.',
    notes: 'Доступ заметкам пользователя.',
    notifications: 'Доступ к оповещениям об ответах пользователю.',
    notify: 'Пользователь разрешил отправлять ему уведомления.',
    offline: 'Доступ к <a href="/pages?oid=-1&amp;p=API">API</a> в любое время со стороннего сервера.',
    pages: 'Доступ к wiki-страницам.',
    photos: 'Доступ к фотографиям.',
    stats: 'Доступ к статистике групп и приложений пользователя, администратором которых он является.',
    status: 'Доступ к статусу пользователя.',
    video: 'Доступ к видеозаписям.'
  },
  errors: {
    1: 'Произошла неизвестная ошибка.',
    2: 'Приложение выключено.',
    3: 'Передан неизвестный метод.',
    4: 'Неверная подпись.',
    5: 'Авторизация пользователя не удалась.',
    6: 'Слишком много запросов в секунду.',
    7: 'Нет прав для выполнения этого действия.',
    8: 'Неверный запрос.',
    9: 'Слишком много однотипных действий.',
    10: 'Произошла внутренняя ошибка сервера.',
    11: 'В тестовом режиме приложение должно быть выключено или пользователь должен быть залогинен.',
    14: 'Требуется ввод кода с картинки (Captcha).',
    15: 'Доступ запрещён.',
    16: 'Требуется выполнение запросов по протоколу HTTPS, т.к. пользователь включил настройку, требующую работу через безопасное соединение.',
    17: 'Требуется валидация пользователя.',
    20: 'Данное действие запрещено для не Standalone приложений.',
    21: 'Данное действие разрешено только для Standalone и Open API приложений.',
    23: 'Метод был выключен.',
    24: 'Требуется подтверждение со стороны пользователя.',
    100: 'Один из необходимых параметров был не передан или неверен.',
    101: 'Неверный API ID приложения.',
    113: 'Неверный идентификатор пользователя.',
    150: 'Неверный timestamp',
    200: 'Доступ к альбому запрещён.',
    201: 'Доступ к аудио запрещён.',
    203: 'Доступ к группе запрещён.',
    300: 'Альбом переполнен.',
    500: 'Действие запрещено. Вы должны включить переводы голосов в настройках приложения.',
    600: 'Нет прав на выполнение данных операций с рекламным кабинетом.',
    603: 'Произошла ошибка при работе с рекламным кабинетом.'
  },

  profile: {
    fields: [
      'verified', 'sex', 'bdate', 'city', 'country', 'home_town',
      'photo_max', 'online', 'has_mobile', 'contacts',
      'site', 'last_seen', 'followers_count',
      'common_count', 'nickname', 'relatives', 'relation',
      'connections', 'exports', 'can_send_friend_request',
      'timezone', 'screen_name', 'maiden_name', 'domain',
      'about'
    ].join(',')
  },

  friends: {
    order: 'hints',
    count: 1000,
    fields: ['domain', 'bdate', 'sex', 'city', 'country'].join(',')
  }
};

vk.auth = merge(vk.auth, {
  scope: Object.keys(vk.permissions).join(',')
})

function vk_call(method, params) {
  params.v = vkontakte.v
  return request(`https://api.vk.com/method/${method}?` + stringify(params))
}

function parseUserData(profile) {
  profile = {data: profile}
  const remap = {
    about: 'about',
    avatar: 'photo_max',
    domain: 'domain',
    facebook: 'facebook',
    forename: 'first_name',
    site: 'link',
    surname: 'last_name',
    twitter: 'twitter'
  }
  each(remap, function (s, t) {
    if (('string' === typeof profile.data[s]) && profile.data[s]) {
      profile[t] = profile.data[s]
    }
  })
  if (profile.link && profile.link.indexOf('http') !== 0) {
    profile.link = 'http://' + profile.link
  }
  if (profile.about && profile.about.length > 400) {
    profile.about = profile.about.slice(0, 400)
  }
  if ('string' === typeof profile.data.bdate) {
    const date = /(\d{1,2})\.(\d{1,2})\.(\d{4})/.exec(profile.data.bdate)
    if (date) {
      profile.birthday = new Date(+date[3], date[2] - 1, +date[1])
    }
  }
  return profile
}

module.exports = {
  describe() {
    return vk
  },

  authorize(req, res) {
    if (req.query.code) {
      const query = {
        scope: Object.keys(vk.permissions),
        client_id: vk.auth.client_id,
        client_secret: vkontakte.secret,
        code: req.query.code,
        redirect_uri: vk.auth.redirect_uri
      }
      if (req.query.state) {
        query.state = +req.query.state
      }
      let body
      let profile
      const userAgent = req.headers['user-agent'] ? req.headers['user-agent'].slice(0, 240) : false
      request('https://oauth.vk.com/access_token?' + stringify(query))
        .then(function (_body) {
          body = JSON.parse(_body)
          body.expires_in = +body.expires_in
          if (!body.expires_in) {
            body.expires_in = 3600 * 24 * 365 * 10
          }
          const user = {
            id: body.user_id,
            ref: req.query.state || root
          }
          return db.entities.user.create(user)
        })
        .then(function () {
          return db.entities.token.create({
            id: body.access_token,
            expires: db.raw(`CURRENT_TIMESTAMP + (${+body.expires_in} * interval '1 second')`),
            user: body.user_id,
            type: 'vkontakte'
          })
        })
        .then(() => vk_call('users.get', merge(vk.profile, {
          user_ids: body.user_id,
          access_token: body.access_token
        })))
        .then(function (vkres) {
          profile = parseUserData(JSON.parse(vkres).response[0])
          return db.entities.user.update({id: body.user_id}, profile)
        })
        .then(() => db.log('user', 'create', pick(profile.data, 'id', 'first_name', 'last_name', 'domain'),
          profile.data.id, req.headers.ip))
        .then(() => res.redirect('/follow?' + stringify(body)))
        .catch((err) => {
          if ('user_pkey' === err.constraint) {
            const where = {user: body.user_id, type: 'vkontakte'}
            const data = {
              id: body.access_token,
              expires: db.raw(`CURRENT_TIMESTAMP + (${+body.expires_in} * interval '1 second')`)
            }
            if (userAgent) {
              data.name = userAgent
            }
            let old
            db.table('token')
              .where(where)
              .select('id', 'expires', 'name')
              .then(function (_old) {
                old = _old[0]
                const q = db.table('token')
                if (old) {
                  return q
                    .where(where)
                    .update(data)
                }
                else {
                  data.type = 'vkontakte'
                  q.insert(data)
                }
              })
              .then(() => db.log('token', 'change', {old, new: body.access_token},
                body.user_id,
                req.headers.ip))
              .then(function () {
                res.redirect('/cabinet?' + stringify(body))
              })
              .catch(salo.express(res))
          }
          else {
            res.json(salo(err))
          }
        })
    }
    else {
      const query = pick(vk.auth, 'client_id', 'redirect_uri', 'display', 'response_type', 'scope')
      if (req.query.ref) {
        query.state = +req.query.ref
      }
      res.redirect('https://oauth.vk.com/authorize?' + stringify(query))
    }
  },

  updateStatus({status}, {user_ids}) {
    function updateFriend() {
      return db.table('friend')
        .where(db.raw('1 = status OR 3 = status'))
        .where('from', this.user.id)
        .count('*')
        .then((r) => {
          const where = {id: this.user.id}
          let q = db.table('user')
            .where(where)
          if (r[0].count >= vkontakte.subscribers) {
            q = q.update({status})
              .then(() => db.table('user').where(where))
          }
          return q.then(r => r[0])
        })
    }

    switch (status) {
      // default:
      case 'subscribed':
        console.log(status, user_ids)
        return user_ids instanceof Array
          ? this.vkontakte('friends.areFriends', {user_ids: user_ids.join(',')})
          .then((r) => {
            if (r.response) {
              const records = []
              r.response.forEach((r) => {
                if (1 === r.friend_status || 3 === r.friend_status) {
                  records.push({
                    from: this.user.id,
                    to: r.user_id,
                    status: r.friend_status
                  })
                }
              })
              const friendTable = db.table('friend')
              records.forEach(function ({from, to}) {
                friendTable.orWhere({from, to})
              })
              console.log(friendTable.toString())
              return friendTable.del()
                .then(function () {
                  const q = db.table('friend')
                    .insert(records)
                  console.log(q.toString())
                  return q
                })
            }
            else {
              this.log('vkontakte', 'error', r)
              return r
            }
          })
          .then(() => updateFriend.call(this))
          : updateFriend.call(this)

      default:
        return {statusCode: 400, request: {status, user_ids}}
    }
  },

  user({id}) {
    return db.entities.user.findOne({id}).then(u => u.data)
  },

  friends() {
    return this.vkontakte('friends.get', merge(vk.friends, {
      user_id: +this.user.id
    }))
  },

  candidates({limit}) {
    if (!this.user) {
      return {statusCode: 401}
    }
    limit = limit ? vkontakte.subscribers : +limit
    if (limit > 30) {
      limit = 30
    }
    let excludes
    let candidates
    let keyed = {}
    return this.vkontakte('friends.get', {user_id: +this.user.id})
      .then((r) => {
        if (r.response) {
          excludes = r.response.items //.map(f => f.id)
          excludes.push(+this.user.id)
          return db.table('sponsor')
            .where('id', 'not in', excludes)
            .where(db.raw('not blocked'))
            .limit(5)
        }
        else {
          this.log('vkontakte', 'error', r)
          throw new Error(r.message)
        }
      })
      .then(function (sponsors) {
        candidates = sponsors
        return db.table('freshman')
          .where('id', 'not in', excludes)
          .where(db.raw('not blocked'))
          .limit(200)
      })
      .then((freshmans) => {
        candidates = candidates.concat(freshmans)
        keyed = keyBy(candidates, 'id')
        return candidates.length > 0
          ? this.vkontakte('friends.areFriends', {user_ids: candidates.map(c => c.id).slice(0, 1000).join(',')})
          : candidates
      })
      .then((r) => {
          if (r.response instanceof Array) {
            const candidates = []
            r.response
              .forEach(function (c) {
                if (0 === c.friend_status || 2 === c.friend_status) {
                  candidates.push(keyed[c.user_id])
                }
              })
            return db.table('out_request')
              .where('from', this.user.id)
              .then((requests) => {
                const o = {
                  need: vkontakte.subscribers > requests.length,
                  enough: vkontakte.subscribers - requests.length,
                  requests,
                  candidates
                }
                return 'subscribed' === this.user.status || o.need
                  ? o :
                  db.table('user')
                    .where({id: this.user.id})
                    .update({status: 'subscribed'})
                    .then(() => o)
              })
          }
          else {
            this.log('vkontakte', 'error', r)
            return r
          }
        }
      )
      .catch(salo)
  },

  invite() {
    if (this.user) {
      return db.table('vip')
        .where('id', '<>', this.user.id)
        .where(db.raw('not blocked'))
        .orderBy('created', 'desc').limit(1000)
        .then(rows => {
          return this.vkontakte('friends.areFriends', {user_ids: rows.map(r => r.id).join(',')})
            .then(function (r) {
              if (r.response) {
                const candidates = []
                r.response.forEach(function (c) {
                  if (0 === c.friend_status || 2 === c.friend_status) {
                    const found = rows.find(r => r.id == c.user_id)
                    if (found) {
                      candidates.push(found)
                    }
                  }
                })
                return {candidates}
              }
              else {
                this.log('vkontakte', 'error', r)
                return r
              }
            })
        })
    }
    return {statusCode: 401}
  },

  checkShare({from}) {
    if (this.user) {
      return db.entities.user.findOne({id: from})
        .then((user) => {
          if (!user) {
            throw new Error(`User ${user.id} not found`)
          }
          return this.vkontakte('wall.get', {owner_id: this.user.id})
            .then(r => ({user, posts: r.response.items}))
        })
        .then(({user, posts}) => {
          const url = user.link || 'http://vk-mm.com/ref/' + user.id
          const post = posts.find(p => p.attachments
          && p.attachments.some(a => 'link' === a.type && url === a.link.url))
          const result = {success: !!post}
          if (post) {
            result.post = post
          }
          const share = {from, to: this.user.id, returning: ['id']}
          let q
          if (result.success) {
            share.id = db.timeId()
            q = db.entities.share.create(share)
          }
          else {
            q = db.entities.share.delete(share)
          }
          return q.then(function (r) {
            if ('DELETE' === r.action && r.success && r.length > 0) {
              result.deleted = r[0].id
            }
            else {
              result.id = share.id
            }
            return result
          })
            .catch(function (err) {
              if (db.UNIQUE_VIOLATION === err.code) {
                return result
              }
              throw err
            })
        })
    }
    else {
      return {statusCode: 401}
    }
  }
};

['users.get', 'friends.areFriends', 'friends.getRecent', 'wall.get'].forEach(function (name) {
  module.exports[name] = function (params) {
    each(params, function (value, key) {
      if (isFinite(value)) {
        params[key] = +value
      }
    })
    console.log(name, params)
    return this.vkontakte(name, params)
  }
})
