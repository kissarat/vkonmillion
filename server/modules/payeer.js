const db = require('schema-db')
const request = require('request-promise')
const salo = require('salo')
const {createHash} = require('crypto')
const {generate} = require('randomstring')
const {isEmpty, pick, defaults} = require('lodash')
const config = require('../../config')
const {$admin} = require('../decorators')
const {stringify} = require('querystring')
const {setImmediate} = require('timers')

function round(value) {
  return (Math.round(value * 100) / 100)
}

function invokePayeer(params) {
  defaults(params, {
    account: config.payeer.account,
    apiId: config.payeer.id,
    apiPass: config.payeer.password
  })
  return request({
    method: 'POST',
    url: 'https://payeer.com/ajax/api/api.php',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: stringify(params)
  })
    .then(JSON.parse);
}

function transfer(to, amount) {
  if (to && config.payeer.account && to === config.payeer.account) {
    return Promise.resolve({myself: true})
  }
  return invokePayeer({
    curOut: 'RUB',
    curIn: 'RUB',
    action: 'transfer',
    sum: round(amount),
    to
  })
    .then(function (r) {
      console.log(r)
      return r
    })
}

function seq(promises) {
  return promises.length > 1
    ? promises[0].then(() => seq(promises.slice(1)))
    : promises[0]
}

module.exports = {
  pay(req, res) {
    const amount = +req.query.amount
    const user_id = +req.query.user_id
    if (!isFinite(amount) || amount <= 0 || !user_id) {
      res
        .status(400)
        .send('Amount must be a positive number')
    }
    else {
      const desc = JSON.stringify(req.query)
      const d = {
        m_shop: config.payeer.shop,
        m_orderid: generate({
          length: 24,
          charset: 'alphanumeric'
        }),
        m_amount: round(amount * (1 + config.payeer.commission)).toFixed(2),
        m_curr: 'RUB',
        m_desc: new Buffer(desc).toString('base64'),
        lang: 'ru'
      }
      const hash = createHash('sha256')
      hash.update([d.m_shop, d.m_orderid, d.m_amount, d.m_curr, d.m_desc, config.payeer.secret].join(':'), 'utf8')
      d.m_sign = hash.digest('hex').toUpperCase()
      res.redirect('https://payeer.com/merchant/?' + stringify(d))
    }
  },

  status(req, res) {
    if (isEmpty(req.query)) {
      res.json({status: 'ACCEPTED'})
    }
    else {
      const d = req.query
      const hash = createHash('sha256')
      const array = [
        d.m_operation_id,
        d.m_operation_ps,
        d.m_operation_date,
        d.m_operation_pay_date,
        d.m_shop,
        d.m_orderid,
        d.m_amount,
        d.m_curr,
        d.m_desc,
        d.m_status,
        config.payeer.secret
      ]
      hash.update(array.join(':'), 'utf8')

      let desc = new Buffer(d.m_desc, 'base64').toString('utf8')
      try {
        desc = JSON.parse(desc)
      }
      catch (ex) {
        desc = null
      }
      const m = {}
      const realAmount = +d.m_amount
      if ('success' != d.m_status) {
        m.status = 'INVALID_STATUS'
        m.error = {message: 'Status must be success'}
      }
      else if (!desc) {
        m.status = 'INVALID_USER_ID'
        m.error = {message: 'Description does not contain user_id'}
      }
      else if (!isFinite(desc.amount) || !isFinite(realAmount) || desc.amount > realAmount) {
        m.status = 'INVALID_AMOUNT'
        m.error = {message: 'Invalid amount'}
      }
      else if (hash.digest('hex').toUpperCase() !== d.m_sign) {
        m.status = 'INVALID_SIGNATURE'
        m.error = {message: 'Invalid signature'}
      }
      else {
        const user_id = +desc.user_id
        const data = {
          id: db.timeId(),
          order_id: d.m_orderid,
          amount: round(desc.amount),
          to: user_id,
          type: 'payment',
          status: 'success',
          data: {
            operation: {
              id: d.m_operation_id,
              ps: d.m_operation_ps,
              date: d.m_operation_date,
              pay_date: d.m_operation_pay_date,
            }
          }
        }
        return db.table('transfer').insert(data)
          .then(function () {
            res.redirect('/success?' + stringify({
                id: data.id,
                order_id: data.order_id
              }))
          })
          .catch(salo.express(res))
      }
      m.statusCode = 403
      m.request = req.query
      res
        .status(403)
        .json(m)
    }
  },

  chess() {
    const req = this
    return new Promise(function (resolve) {
      function alert(type, message, vars, error) {
        if (error) {
          error = salo(error)
        }
        const answer = {
          statusCode: error ? 500 : 202,
          type, message, vars, error
        }
        const rs = () => resolve(answer)
        return req.log('transfer', type, {message, vars})
          .then(rs, rs)
      }

      let transaction = req.query.id ? req.query.id : db.timeId()
      transaction = +transaction

      function accrue(receives) {
        receives = receives instanceof Array ? receives : [receives]
        const transfers = [{
          amount: round(config.chess.in - config.chess.out),
          status: 'success',
          from: req.user.id,
          type: 'support',
          transaction
        }]
          .concat(receives.map(({id, amount, is_active}) => ({
            amount: round(amount),
            from: req.user.id,
            status: 'success',
            to: id && is_active ? id : null,
            type: is_active ? 'accrue' : 'write-off',
            transaction
          })))

        transfers.forEach(function (t, i) {
          t.id = transaction + (i + 1) * 8192
        })

        const q = db.table('transfer')
          .insert(transfers, 'id')
        console.log(q.toString())
        return q.then(() => receives)
      }

      function accrueAndAnswer(type, m, vars, data) {
        return accrue({amount: round(config.chess.out)})
          .then(function () {
            alert(type, m, vars, data)
          })
      }

      function saveWithdrawTransfer(r, from, amount, wallet, more) {
        const data = r.historyId ? {operation: {id: r.historyId}} : r
        if (more) {
          defaults(data, more)
        }
        const withdraw = {
          id: db.timeId(),
          type: 'withdraw',
          status: r.historyId || r.myself ? 'success' : 'fail',
          from,
          amount,
          wallet,
          transaction,
          data
        }
        const q = db.table('transfer').insert(withdraw)
        console.log(q.toString())
        return q.then(() => withdraw)
      }

      function _chess(user) {
        db.entities.user_view.findOne({id: user.ref})
          .then(function (ref) {
            if (ref) {
              const active = new Date(ref.active)
              if (active.getTime() > Date.now()) {
                const q = db.table('sponsor')
                  .innerJoin('active_payment', 'sponsor.id', 'active_payment.id')
                  .select('sponsor.id', 'sponsor.level', 'sponsor.payeer', 'active_payment.active')
                  .where('root', req.user.id)
                  .orderBy('level', 'asc')
                  .limit(2)
                console.log(q.toString())
                q
                  .then(function (sponsors) {
                    if (sponsors.length >= 2) {
                      const _ref = db.table('vip')
                        .where('ref', ref.id)
                        .count('*')
                      console.log(_ref.toString())
                      _ref
                        .then(function (r) {
                          const receiver = sponsors[r[0].count % 2 ? 0 : 1]
                          if (receiver) {
                            const active = new Date(ref.active)
                            if (active.getTime() > Date.now()) {
                              accrue({id: receiver.id, amount: config.chess.out, is_active: true})
                                .then(function () {
                                  if (receiver.payeer) {
                                    transfer(receiver.payeer, config.chess.out * (1 + config.payeer.commission))
                                      .then(r => saveWithdrawTransfer(r, receiver.id, config.chess.out, receiver.payeer))
                                      .then(function (withdraw) {
                                        if ('success' === withdraw.status) {
                                          alert('success', 'Success')
                                        }
                                        else if (withdraw.data && withdraw.data.errors instanceof Array) {
                                          alert('error', withdraw.data.errors[0])
                                        }
                                        else {
                                          alert('error', 'Unknown error', null, withdraw.data)
                                        }
                                      })
                                      .catch(function (err) {
                                        alert('error', `An error occurred when money transfer to the sponsor id{id}`, {id: receiver.id}, err)
                                      })
                                  }
                                  else {
                                    alert('error', `The sponsor id{id} has no wallet`, {id: receiver.id})
                                  }
                                })
                                .catch(function (err) {
                                  alert('error', 'An error occurred during the creation of the accrual', null, err)
                                })
                            }
                            else {
                              accrueAndAnswer('error', 'Sponsor is id{id} not active', {id: receiver.id})
                            }
                          }
                          else {
                            accrueAndAnswer('error', 'Sponsor is not found')
                          }
                        })
                        .catch(function (err) {
                          alert('error', 'An error occurred when obtaining a list of your sponsor referrals', null, err)
                        })
                    }
                    else {
                      alert('error', 'Your sponsor has no sponsor')
                    }
                  })
                  .catch(function (err) {
                    alert('error', 'An error occurred while obtaining the list of sponsors', null, err)
                  })
              }
              else {
                accrueAndAnswer('error', 'Your sponsor id{id} is not active', {id: ref.id})
              }
            }
            else {
              accrueAndAnswer('error', 'You do not have a sponsor')
            }
          })
          .catch(function (err) {
            alert('error', 'An error occurred while getting a sponsor', null, err)
          })
      }

      if (req.user) {
        let withdraw
        return db.entities.user_view.findOne({id: req.user.id})
          .then(function (user) {
            if (user && user.balance >= config.chess.in) {
              if (user.chess) {
                db.table('linear')
                  .where('root', req.user.id)
                  .orderBy('level')
                  .then(accrue)
                  .then(function (levels) {
                    alert('success', 'Money received')
                    setImmediate(function () {
                      seq(levels
                        .filter(l => l.is_active && l.payeer)
                        .map(
                          l => transfer(l.payeer, l.amount)
                            .then(r => saveWithdrawTransfer(r, l.id, l.amount, l.payeer, {level: l.level}))
                        ))
                        .catch(function (err) {
                          alert('error', 'Error while withdraw', null, err)
                        })
                    })
                  })
              }
              else {
                db.entities.user.update({id: user.id}, {chess: transaction})
                  .then(() => _chess(user))
                  .catch(function (err) {
                    alert('error', 'An error occurred updating user', null, err)
                  })
              }
            }
            else {
              alert('error', 'You have insufficient funds')
            }
          })
          .catch(function (err) {
            alert('error', 'An error occurred while getting user information', null, err)
          })
      }
      else {
        resolve({statusCode: 401})
      }
    })
  },

  history: $admin(function (params) {
    params = pick(params, 'sort', 'count', 'from', 'to', 'type', 'append')
    defaults(params, {
      count: 1000
    })
    return invokePayeer(defaults(params, {
      action: 'history'
    }))
      // .then(function (r) {
      //   if (r.history instanceof Array) {
      //     r.history.sort(function (a, b) {
      //       a = new Date(a.date).getTime()
      //       b = new Date(b.date).getTime()
      //       return ('desc' === params.sort ? -1 : 1) * (b - a)
      //     })
      //   }
      //   return r
      // })
  }),

  get: $admin(function ({id}) {
    return invokePayeer({
      action: 'historyInfo',
      historyId: id
    })
  }),

  balance: $admin(function () {
    return invokePayeer({
      action: 'balance',
    })
  })
}
