const db = require('schema-db')

module.exports = {
  index() {
    if (this.user) {
      return db.table('transfer')
        .orWhere('from', this.user.id)
        .orWhere('to', this.user.id)
        .orderBy('id', 'desc')
        .then(function (rows) {
          rows.forEach(function (row) {
            row.time = new Date(row.id / (1000 * 1000)).toLocaleString()
          })
          return rows
        })
    }
    else {
      return {statusCode: 401}
    }
  },

  get({id}) {
    return db.table('transfer')
      .where('order_id', id)
      .select('id', 'amount', 'order_id')
      .then(r => r[0])
  }
}
