const db = require('schema-db')
const bcrypt = require('promised-bcrypt')
const config = require('../../config')
const {each, defaults} = require('lodash')
const {$admin} = require('../decorators')

const publicColumns = ['id', 'surname', 'forename', 'avatar', 'about', 'link', 'ref', 'redirect',
  'skype', 'facebook', 'odnoklassniki', 'twitter', 'created', 'admin']

function clear(data) {
  each(data, function (value, key) {
    if (undefined === value || null === value) {
      delete data[key]
    }
  })
  return data
}

function tree(id, limit) {
  const q = db
    .table('referral')
    .join('active_payment', 'referral.id', 'active_payment.id')
    .select('referral.id', 'referral.forename', 'referral.surname', 'referral.created', 'referral.blocked',
      'referral.level', 'referral.ref', 'active_payment.active')
    .where('root', id)
    .orderBy('level')
    .orderBy('created')
  if (limit) {
    q.where('level', '<=', limit)
  }
  console.log(q.toString())
  return q.then(function (rows) {
    if (0 === rows.length) {
      return {statusCode: 404}
    }

    function build(a) {
      const root = {
        id: a.id,
        name: a.forename + ' ' + a.surname,
        created: a.created,
        blocked: a.blocked,
        level: a.level,
        is_active: new Date(a.active).getTime() > Date.now()
      }
      const children = rows
        .filter(row => row.ref === root.id)
        .map(row => build(row))
      if (children.length > 0) {
        root.children = children
      }
      return root
    }

    const root = rows.find(row => 0 === row.level)
    return root ? build(root) : {
      statusCode: 500,
      error: {
        message: 'Root not found'
      }
    }
  })
}

module.exports = {
  login({id, password}) {
    let token
    return bcrypt.hash(password)
      .then((_token) => {
        token = _token
        db.entities.user_token.findOne({id: token, user_id: id})
      })
      .then((user) => {
        if (token) {
          user.token = token
          return db.entites.token
            .update({id: token}, {user: user.id})
            .then(() => user)
        }
        return user
      })
  },

  logout() {
    return db.entities.token.update(
      {id: this.user.token, returning: 'user'},
      {user: null}
    )
  },

  me() {
    return this.user
      ? db.entities.user_view.findOne({id: this.user.id})
      .then(u => {
        u.token = this.user.token
        return u.ref && u.ref
          ? db.entities.user.findOne({id: u.ref})
          .then(r => {
            u.ref = r.visible ? r : null
            return u
          })
          : u
      })
      : {statusCode: 401}
  },

  last({limit}) {
    if (!this.user) {
      return {statusCode: 401}
    }
    if (limit && (!isFinite(limit) || limit < 0)) {
      return {statusCode: 40}
    }
    limit = +limit
    if (limit > 10) {
      limit = this.user.admin ? 1000 : 10
    }
    return db.table('user')
      .orderBy('created', 'desc')
      .limit(limit)
      .then(function (users) {
        users.forEach(function (user) {
          clear(user)
          delete user.data
        })
        return users
      })
  },

  save({id}, data) {
    if (!this.user) {
      return {statusCode: 401}
    }

    if (!id) {
      id = this.user.id
    }

    if (this.user.id == id) {
      return db.entities.user.update({id, returning: ['id']}, data)
    }

    return {statusCode: 403}
  },

  signup({nick, phone, email, password}) {
    return entities.user
      .create({nick, phone, email, returning: ['id']})
      .then(({id}) => ({user: id, id: hash(password)}))
      .then((token) => entities.token.create(token))
  },

  get({id}) {
    return db.table('user')
      .select(publicColumns)
      .where('id', +id)
      .then(function (r) {
        if (r.length > 0) {
          return r[0]
        }
        return {statusCode: 404}
      })
  },

  goref({from, id, referer}) {
    const data = {to: id, from}
    data.ip = this.headers.ip
    if (this.headers['user-agent']) {
      data.name = this.headers['user-agent']
    }
    data.referer = referer || this.headers.referer
    return db.table('goref')
      .insert(data)
      .catch(function (err) {
        if (db.UNIQUE_VIOLATION === err.code) {
          return {
            statusCode: 409,
            error: {
              message: 'Already saved'
            }
          }
        }
        else {
          throw err
        }
      })
  },

  structure({view}) {
    const allowable = ['sponsor', 'referral']
    if (allowable.indexOf(view) < 0) {
      throw new Error(`Parameter view must be ${allowable.join(' or ')}`)
    }
    if (this.user) {
      return db.table(view)
        .where('root', this.user.id)
        .where(db.raw('"level" <= 5'))
        .where(db.raw('"level" > 0'))
        .orderBy('level', 'asc')
        .select(publicColumns)
    }
    else {
      return {statusCode: 401}
    }
  },

  all: $admin(function (params) {
    defaults(params, {order: '-created'})
    const booleanColumns = ['admin', 'blocked', 'chess']
    booleanColumns.forEach(function (name) {
      if (name in params) {
        params[name] = !!(++params[name])
      }
    })

    return db.entities.user.read(params)
      .then(rows => rows.map(function (user) {
        delete user.data
        clear(user)
        return user
      }))
  }),

  shareCards() {
    if (this.user) {
      const columns = publicColumns.join(',')
      const sql = `
      WITH
      s AS (SELECT "from" FROM share WHERE "to" = ?),
      card AS (
        (SELECT ${columns} FROM sponsor WHERE root = ? AND NOT blocked ORDER BY level LIMIT 3)
        UNION
        (SELECT ${columns} FROM vip WHERE NOT blocked ORDER BY shares, random() LIMIT 100)
      )
      SELECT * FROM card`
      // console.log(sql)
      return db.raw(sql, [this.user.id, this.user.id])
        .then(r => {
          const cards = r.rows.map(clear)
          return db.table('share').where('to', this.user.id).count('*')
            .then(c => ({
              count: +c[0].count,
              cards
            }))
        })
    }
    else {
      return {statusCode: 401}
    }
  },

  tree({id}) {
    id = +id
    if (!this.user) {
      return {statusCode: 401}
    }
    if (!this.user.admin && id != this.user.id) {
      return {statusCode: 403}
    }
    else if (id) {
      return this.user.admin ? tree(id) : tree(id, 5)
    }
    else if (this.user.admin) {
      return db
        .table('user')
        .select('id', 'surname', 'forename', 'active')
        .where(db.raw('ref is null'))
        .orderBy('created')
        .then(function (rows) {
          if (rows.length !== 1) {
            return {
              statusCode: 500,
              rows,
              error: {
                message: 'Invalid number of roots'
              }
            }
          }
          else {
            return tree(rows[0].id)
          }
        })
    }
    else {
      return tree(this.user.id, 5)
    }
  }
}
