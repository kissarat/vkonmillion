module.exports = {
  home: require('./home'),
  token: require('./token'),
  user: require('./user'),
  vkontakte: require('./vkontakte'),
  payeer: require('./payeer'),
  transfer: require('./transfer')
}
