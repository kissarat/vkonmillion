const {generate} = require('randomstring')
const {entities, raw} = require('schema-db')

module.exports = {
  create({token, type}) {
    return entities.token.create({
      id: token || generate({charset: 'alphanumeric', length: 48}),
      user: this.user.id || null,
      type: type || 'plain',
      name: this.headers ? this.headers['user-agent'] : new Date()
    })
  },

  expire({token}) {
    return entities.token.update(
      {id: token, returning: ['id', 'user']},
      {time: raw('CURRENT_TIMESTAMP')}
    )
  }
}
