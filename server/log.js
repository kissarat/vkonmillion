const db = require('schema-db')
const body = require('body')
const {each} = require('lodash')
const salo = require('salo')
const qs = require('querystring')

let config = require('../config').http.log

module.exports = function logger(req, res, next) {
  function save(req, data) {
    db.raw('INSERT INTO request(url, method, headers, data) VALUES (?,?,?,?)',
      [req.url, req.method, req.headers, data])
      .then(function () {
        next()
      })
      .catch(salo.express(res))
  }

  let match = config.methods.indexOf(req.method) >= 0
  match &= !config.exclude.urls.some(m => m instanceof RegExp
    ? m.test(req.url)
    : req.url.indexOf(m) === 0)

  if (match) {
    each(config.exclude.headers, function (name) {
      delete req.headers[name]
    })
    body(req, {limit: config.limits.data}, function (err, body) {
      if (err) {
        res
          .status(400)
          .json(salo(err))
      }
      else {
        const type = req.headers['content-type']
        if (body && 'string' === typeof type) {
          if (type.indexOf('application/json') >= 0) {
            try {
              req.body = JSON.parse(body)
            }
            catch (ex) {
              return res
                .status(400)
                .json(salo(ex))
            }
          }
          else if (type.indexOf('application/x-www-form-urlencoded') >= 0) {
            try {
              req.body = qs.parse(body)
            }
            catch (ex) {
              return res
                .status(400)
                .json(salo(ex))
            }
          }
        }
        save(req, body || null)
      }
    })
  }
  else {
    next()
  }
}
