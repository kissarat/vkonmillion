function $admin(cb) {
  return function () {
    if (!this.user) {
      return {statusCode: 401}
    }
    if (!this.user.admin) {
      return {statusCode: 403}
    }
    else {
      return cb.apply(this, arguments)
    }
  }
}

module.exports = {$admin}
