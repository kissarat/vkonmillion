const config = require('../config')
const modules = require('./modules')
const request = require('request-promise')
const salo = require('salo')
const {each, isObject} = require('lodash')
const db = require('schema-db')
const {stringify} = require('querystring')

function reflectionRoute(req, res, next) {
  if (req.accepts('json')) {
    const result = {}
    each(modules, function (module, name) {
      const moduleResult = {}
      each(module, function (fn, name) {
        moduleResult[name] = fn instanceof Function
          ? /^[\w\s]+\(([^)]*)/.exec(fn.toString())[1]
          : fn
      })
      result[name] = moduleResult
    })
    res.json(result)
  }
  else {
    next()
  }
}

function errorRoute(err, req, res, next) {
  if (err && req.accepts('json')) {
    res.json(salo(err))
  }
  else {
    next()
  }
}

function elseRoute(req, res) {
  if (req.accepts('html')) {
    res.sendFile(config.static.dir + '/react.html')
  }
  else if (req.accepts('json')) {
    res
      .status(404)
      .json({
        statusCode: 404,
        status: 'UNKNOWN_ROUTE',
        url: req.url,
        error: {
          message: 'Unknown route'
        }
      })
  }
  else {
    res
      .status(406)
      .json({statusCode: 406})
  }
}

function vkontakte(method, params = {}) {
  params.v = config.vkontakte.v
  if (this.user && this.user.token) {
    params.access_token = this.user.token
  }
  const url = `https://api.vk.com/method/${method}?` + stringify(params)
  console.log(url)
  return request(url).then(JSON.parse)
}

function log(entity, action, data, user, ip) {
  return db.log(entity, action, data, user || (this.user ? this.user.id : null), ip || this.headers.ip)
}

function r(cb) {
  if (!(cb instanceof Function)) {
    throw new Error('Handler is not a function ' + cb.name)
  }
  return function (req, res, next) {
    function json(data) {
      if (isObject(data)) {
        if (data.statusCode) {
          res.status(data.statusCode)
        }
        res.json(data)
      }
      else {
        res.json({error: {message: `${data} is not object`}})
      }
    }

    if (req.accepts('json')) {
      try {
        req.vkontakte = vkontakte
        req.log = log
        const data = cb.call(req, req.query || req.body, req.body)

        if (data) {
          if (data.then instanceof Function) {
            data
              .then(function (data) {
                return json(data)
              })
              .catch(salo.express(res))
          }
          else {
            json(data)
          }
        }
        else {
          res.end()
        }
      }
      catch (ex) {
        res.json(salo(ex))
      }
    }
    else {
      next()
    }
  }
}

function route(req, res, next) {
  const isJSON = 'string' === typeof req.headers.accept && req.headers.accept.indexOf('application/json') >= 0
  if ('dev' === config.mode || isJSON) {
    let route = req.url.split('?')[0].split('/')
    if (route.length < 2) {
      return next()
    }
    route = route.slice('~' === route[1][0] ? 2 : 1)
    if (route.length >= 2 && 'function' === typeof modules[route[0]][route[1]]) {
      r(modules[route[0]][route[1]])(req, res, next)
    }
    else {
      next()
    }
  }
  else {
    next()
  }
}

module.exports = {route, reflectionRoute, errorRoute, elseRoute, r}
