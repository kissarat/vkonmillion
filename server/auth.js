const db = require('schema-db')
const salo = require('salo')

module.exports = function (req, res, next) {
  let token = /(\/~[^/]+)/.exec(req.url)
  if (token) {
    token = token[1]
    req.url = req.url.slice(token.length)
    req.token = token.slice(2)
    db.entities.user_token.findOne({token: req.token}).then(function(user) {
      req.user = user
      next()
    })
      .catch(salo.express(res))
  }
  else {
    next()
  }
}
