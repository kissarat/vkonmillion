#!/usr/bin/env bash

export ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export KNEX=`node $ROOT/config.js database`
export PS1="\W-\t\\$ \[$(tput sgr0)\]"

bash
