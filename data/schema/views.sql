CREATE OR REPLACE VIEW user_token AS
  SELECT
    u.id,
    t.id AS token,
    t.expires,
    u.status,
    u.blocked,
    u.admin
  FROM "user" u
    LEFT JOIN token t ON u.id = t."user"
  WHERE expires IS NULL OR t.expires > CURRENT_TIMESTAMP;

CREATE OR REPLACE VIEW sponsor AS
  WITH RECURSIVE r(id, ref, root, level) AS (
    SELECT
      u."id",
      u."ref",
      u."id" AS root,
      0      AS level
    FROM "user" u
    UNION
    SELECT
      u."id",
      u."ref",
      r.root,
      r.level + 1 AS level
    FROM "user" u
      JOIN r ON u.id = r."ref")
  SELECT
    r.root,
    r.level,
    r."id",
    r."ref",
    u."surname",
    u."forename",
    u."avatar",
    u."about",
    u."link",
    u."redirect",
    u."skype",
    u."facebook",
    u."odnoklassniki",
    u."twitter",
    u."payeer",
    u."visible",
    u."active",
    u."blocked",
    u."created",
    u."chess",
    u."admin"
  FROM r
    JOIN "user" u ON r.id = u.id
  WHERE "level" > 0 AND u.visible
  ORDER BY level;

CREATE OR REPLACE VIEW referral AS
  WITH RECURSIVE r(id, ref, root, level) AS (
    SELECT
      u."id",
      u."ref",
      u."id" AS root,
      0      AS level
    FROM "user" u
    UNION
    SELECT
      u."id",
      u."ref",
      r.root,
      r.level + 1 AS level
    FROM "user" u
      JOIN r ON u."ref" = r."id")
  SELECT
    r.root,
    r.level,
    r."id",
    r."ref",
    u."surname",
    u."forename",
    u."avatar",
    u."about",
    u."link",
    u."redirect",
    u."skype",
    u."facebook",
    u."odnoklassniki",
    u."twitter",
    u."payeer",
    u."visible",
    u."active",
    u."blocked",
    u."chess",
    u."created",
    u."admin"
  FROM r
    JOIN "user" u ON r.id = u.id;

CREATE OR REPLACE VIEW transfer_now AS
  SELECT
    "to",
    amount,
    type,
    CURRENT_TIMESTAMP - (TIMESTAMP 'epoch' + (id / 1000) * INTERVAL '1 microsecond') AS passed
  FROM transfer
  WHERE status = 'success';

CREATE OR REPLACE VIEW income AS
  SELECT
    id,
    (SELECT sum(amount)
     FROM transfer_now tnm
     WHERE tnm."to" = u.id
           AND type = 'accrue')                                                 AS "amount",
    (SELECT sum(amount)
     FROM transfer_now tnm
     WHERE tnm."to" = u.id AND passed < INTERVAL '1 month' AND type = 'accrue') AS "last_month",
    (SELECT sum(amount)
     FROM transfer_now tnm
     WHERE tnm."to" = u.id AND passed < INTERVAL '1 day' AND type = 'accrue')   AS "today"
  FROM "user" u;

CREATE OR REPLACE VIEW active_payment AS
  WITH u AS (
      SELECT
        id,
        created,
        active,
        extract(EPOCH FROM (CURRENT_TIMESTAMP - u1.created)) / (3600 * 24 * 30) AS months,
        (SELECT sum(t.amount)
         FROM transfer_now t
         WHERE t."to" = u1.id
               AND t.type = 'payment')                                          AS amount
      FROM "user" u1
  )
  SELECT
    id,
    months,
    coalesce(amount, 0)              AS amount,
    coalesce(u.amount / u.months, 0) AS "per_month",
    coalesce(u.active, (u.created - INTERVAL '1 day') + (u.amount / c.float) * 31 * INTERVAL '1 day',
             TIMESTAMP 'epoch')      AS active
  FROM u
    JOIN config c ON c.name = 'chess_in';

CREATE OR REPLACE VIEW freshman AS
  WITH t AS (
      SELECT p.id
      FROM "user" c
        JOIN "user" p ON c.ref = p.id
  )
  SELECT
    u.id,
    forename,
    surname,
    avatar,
    visible,
    blocked,
    p.active
  FROM "user" u
    JOIN active_payment p ON u.id = p.id
  WHERE u.id NOT IN (SELECT id
                   FROM t);

CREATE OR REPLACE VIEW balance AS
  WITH t AS (
    SELECT
      "to" AS id,
      amount
    FROM transfer it
    WHERE "to" IS NOT NULL AND it.status = 'success'
    UNION ALL
    SELECT
      "from" AS id,
      -amount
    FROM transfer ot
    WHERE "from" IS NOT NULL AND ot.status = 'success'
  )
  SELECT
    id,
    sum(amount) AS balance
  FROM t
  GROUP BY id;

CREATE OR REPLACE VIEW friend_passed AS
  SELECT
    "to",
    "from",
    (CURRENT_TIMESTAMP - created) AS passed
  FROM "friend"
  WHERE status = 1 OR status = 3;

CREATE OR REPLACE VIEW in_request AS
  SELECT
    "to",
    "passed",
    u.*
  FROM friend_passed f
    JOIN "user" u ON f."to" = u.id;

CREATE OR REPLACE VIEW out_request AS
  SELECT
    "from",
    "passed",
    u.*
  FROM friend_passed f
    JOIN "user" u ON f."to" = u.id;

CREATE OR REPLACE VIEW friend_view AS
  SELECT
    id,
    (SELECT count(*)
     FROM in_request ir
     WHERE ir."to" = u.id)                               AS friends,
    (SELECT count(*)
     FROM in_request ir
     WHERE ir."to" = u.id AND passed < INTERVAL '1 day') AS friends_today
  FROM "user" u;

CREATE OR REPLACE VIEW user_view AS
  SELECT
    u."id",
    U."ref",
    u."status",
    u."surname",
    u."forename",
    u."avatar",
    u."about",
    u."link",
    u."redirect",
    u."skype",
    u."facebook",
    u."odnoklassniki",
    u."twitter",
    u."payeer",
    u."visible",
    u."blocked",
    u."created",
    u."chess",
    u."admin",
    u."email",
    u."phone",
    u."instagram",
    coalesce(b.balance, 0)                                                   AS "balance",
    coalesce(a.amount, 0)                                                    AS "income",
    coalesce(a.last_month, 0)                                                AS "income_last_month",
    coalesce(a.today, 0)                                                     AS "income_today",
    p.per_month,
    p.active,
    fv.friends,
    fv.friends_today,
    (SELECT count(*)
     FROM goref g
     WHERE g."to" = u.id)                                                    AS goref,
    (SELECT count(*)
     FROM goref gd
     WHERE gd."to" = u.id AND CURRENT_TIMESTAMP - "time" < INTERVAL '1 day') AS goref_day
  FROM "user" u
    LEFT JOIN balance b ON u.id = b.id
    JOIN active_payment p ON u.id = p.id
    JOIN income a ON u.id = a.id
    JOIN friend_view fv ON u.id = fv.id;

CREATE OR REPLACE VIEW vip AS
  SELECT
    u.*,
    (SELECT count(*)
     FROM share s
     WHERE s."from" = u.id) AS shares
  FROM user_view u
  WHERE active > CURRENT_TIMESTAMP;

CREATE OR REPLACE VIEW request_ip AS
  SELECT
    id,
    (headers ->> 'ip') :: INET AS ip,
    url
  FROM request;

CREATE OR REPLACE VIEW linear AS
  SELECT
    s.root,
    s.level,
    l.percent * c.float          AS amount,
    s.id,
    a.active,
    a.active > CURRENT_TIMESTAMP AS is_active,
    s.payeer
  FROM sponsor s
    JOIN linear_level l ON s.level = l.id
    JOIN active_payment a ON s.id = a.id
    JOIN config c ON c.name = 'chess_out';

CREATE VIEW "admin" AS
  SELECT
    id,
    surname,
    forename,
    ref,
    payeer,
    balance,
    active,
    blocked,
    chess
  FROM "user_view"
  WHERE admin;

CREATE VIEW "chess" AS
  SELECT
    id,
    surname,
    forename,
    ref,
    payeer,
    balance,
    active,
    blocked,
    chess
  FROM "user_view"
  WHERE chess IS NOT NULL;

CREATE VIEW "named_token" AS
  SELECT
    u.id,
    surname,
    forename,
    balance,
    active,
    blocked,
    t.id AS token
  FROM "user_view" u
    JOIN token t ON u.id = t.user;
