CREATE TYPE user_status AS ENUM ('created', 'subscribed');
CREATE DOMAIN user_id INT;

CREATE TABLE "user" (
  id            user_id PRIMARY KEY,
  domain        VARCHAR(64),
  surname       VARCHAR(64),
  forename      VARCHAR(64),
  avatar        VARCHAR(128),
  payeer        VARCHAR(10),
  visible       BOOLEAN     NOT NULL DEFAULT TRUE,
  blocked       BOOLEAN     NOT NULL DEFAULT FALSE,
  chess         BIGINT,
  active        DATE,
  birthday      DATE,
  created       TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ref           user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  redirect      user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  status        user_status NOT NULL DEFAULT 'created',
  admin         BOOLEAN     NOT NULL DEFAULT FALSE,
  data          JSON,
  link          VARCHAR(240),
  email         VARCHAR(240),
  skype         VARCHAR(240),
  phone         VARCHAR(240),
  facebook      VARCHAR(240),
  odnoklassniki VARCHAR(240),
  twitter       VARCHAR(240),
  instagram     VARCHAR(240),
  about         VARCHAR(300)
);

CREATE TABLE friend (
  "from"  user_id   NOT NULL,
  "to"    user_id   NOT NULL,
  status  SMALLINT  NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE ("from", "to")
);

CREATE TYPE token_type AS ENUM ('plain', 'password', 'vkontakte');

CREATE TABLE token (
  id      VARCHAR(240),
  "user"  user_id    NOT NULL REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "type"  token_type NOT NULL DEFAULT 'plain',
  created TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  expires TIMESTAMP,
  name    VARCHAR(240)
);

CREATE TABLE log (
  id     BIGINT PRIMARY KEY,
  entity VARCHAR(16) NOT NULL,
  action VARCHAR(32) NOT NULL,
  ip     INET,
  "user" user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  data   JSON
);

CREATE TYPE transfer_type AS ENUM ('payment', 'withdraw', 'accrue', 'support', 'write-off');
CREATE TYPE transfer_status AS ENUM ('created', 'success', 'fail');

CREATE TABLE transfer (
  id          BIGINT PRIMARY KEY,
  "from"      user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"        user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  amount      DECIMAL(8, 2)   NOT NULL,
  "type"      transfer_type,
  "status"    transfer_status NOT NULL DEFAULT 'created',
  order_id    CHAR(24) UNIQUE,
  wallet      VARCHAR(10),
  transaction BIGINT,
  data        JSON
);

CREATE TABLE goref (
  time    TIMESTAMP NOT NULL PRIMARY KEY DEFAULT CURRENT_TIMESTAMP,
  ip      INET      NOT NULL,
  referer VARCHAR(240),
  "from"  user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"    user_id   NOT NULL REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  name    VARCHAR(240),
  UNIQUE ("from", "to"),
  UNIQUE ("ip", "to")
);

CREATE TYPE request_method AS ENUM ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT');

CREATE TABLE request (
  id      BIGSERIAL PRIMARY KEY,
  time    TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  url     VARCHAR(4096)  NOT NULL,
  method  request_method NOT NULL,
  headers JSON           NOT NULL,
  data    TEXT
);

CREATE TABLE config (
  name  VARCHAR(24) PRIMARY KEY,
  float FLOAT4
);

INSERT INTO config (name, float) VALUES
  ('chess_in', 600),
  ('chess_out', 500);

CREATE TABLE share (
  id     BIGINT PRIMARY KEY,
  "from" user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"   user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE ("from", "to")
);

CREATE TABLE linear_level (
  id      INT   NOT NULL,
  percent FLOAT NOT NULL
);

INSERT INTO linear_level VALUES
  (5, 0.25),
  (4, 0.20),
  (3, 0.15),
  (2, 0.15),
  (1, 0.25);

CREATE TABLE menu (
  id   SMALLINT PRIMARY KEY,
  name VARCHAR(48)  NOT NULL,
  url  VARCHAR(256) NOT NULL
);

CREATE TABLE product (
  id    SERIAL PRIMARY KEY,
  name  VARCHAR(48)   NOT NULL,
  price DECIMAL(8, 2) NOT NULL
);

CREATE TABLE discount (
  id      SERIAL PRIMARY KEY,
  product INT           NOT NULL REFERENCES product (id),
  amount  DECIMAL(5, 1) NOT NULL,
  price   DECIMAL(8, 2) NOT NULL
);

COMMENT ON TABLE log IS 'Journal';
COMMENT ON COLUMN log.ip IS 'IP';
