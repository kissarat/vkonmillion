CREATE TYPE transfer_type AS ENUM ('payment', 'withdraw', 'accrue', 'support', 'write-off');

ALTER TABLE transfer
  ADD COLUMN "type" transfer_type
