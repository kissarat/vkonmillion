CREATE TYPE transfer_status AS ENUM ('created', 'success', 'fail');

ALTER TABLE transfer
  ADD COLUMN "status" transfer_status NOT NULL DEFAULT 'created'
