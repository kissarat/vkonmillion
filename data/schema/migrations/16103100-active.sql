ALTER TABLE transfer
  ALTER COLUMN "from" DROP NOT NULL;
ALTER TABLE transfer
  ALTER COLUMN "to" DROP NOT NULL;

CREATE TABLE goref (
  time    TIMESTAMP NOT NULL PRIMARY KEY DEFAULT CURRENT_TIMESTAMP,
  ip      INET      NOT NULL,
  referer VARCHAR(240),
  "from"  user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"    user_id   NOT NULL REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  name    VARCHAR(240),
  UNIQUE ("from", "to"),
  UNIQUE ("ip", "to")
);

CREATE OR REPLACE VIEW transfer_now AS
  SELECT
    "to",
    amount,
    CURRENT_TIMESTAMP - (TIMESTAMP 'epoch' + (id / 1000) * INTERVAL '1 microsecond') AS passed
  FROM transfer;

CREATE OR REPLACE VIEW active AS
  SELECT
    id,
    (SELECT sum(amount)
     FROM transfer_now tnm) AS "income",
    (SELECT sum(amount)
     FROM transfer_now tnm
     WHERE tnm."to" = u.id AND passed < INTERVAL '1 month') AS "month",
    (SELECT sum(amount)
     FROM transfer_now tnm
     WHERE tnm."to" = u.id AND passed < INTERVAL '1 day')   AS "day"
  FROM "user" u;

CREATE OR REPLACE VIEW balance AS
  WITH t AS (
    SELECT
      "to" AS id,
      amount
    FROM transfer
    WHERE "to" IS NOT NULL
    UNION ALL
    SELECT
      "from" AS id,
      -amount
    FROM transfer
    WHERE "from" IS NOT NULL
  )
  SELECT
    id,
    sum(amount) AS balance
  FROM t
  GROUP BY id;

CREATE OR REPLACE VIEW in_request AS
  SELECT
    "to"                          AS id,
    (CURRENT_TIMESTAMP - created) AS passed
  FROM friend
  WHERE status = 1 OR status = 3;

CREATE OR REPLACE VIEW friend_view AS
  SELECT
    id,
    (SELECT count(*)
     FROM in_request)                                  AS friends,
    (SELECT count(*)
     FROM in_request ir
     WHERE ir.id = u.id AND passed < INTERVAL '1 day') AS friends_day
  FROM "user" u;

CREATE OR REPLACE VIEW user_view AS
  SELECT
    u.*,
    b.balance,
    coalesce(a.income, 0)                                                    AS "income",
    coalesce(a.month, 0)                                                     AS "month",
    coalesce(a.day, 0)                                                       AS "day",
    fv.friends,
    fv.friends_day,
    (SELECT count(*)
     FROM goref g
     WHERE g."to" = u.id)                                                    AS goref,
    (SELECT count(*)
     FROM goref gd
     WHERE gd."to" = u.id AND CURRENT_TIMESTAMP - "time" < INTERVAL '1 day') AS goref_day
  FROM "user" u
    LEFT JOIN balance b ON u.id = b.id
    JOIN active a ON u.id = a.id
    JOIN friend_view fv ON u.id = fv.id;
